# ImageSearch

## Overview

This is a demo project. The main focus in on architecture, not on UI.

It query images on Social Media

You can see the full spec [here](./spec/spec.txt) 

### Demo 

Screenshot from ./spec/demo/:

with picasso indicator
(Bitbucket cannot resize embeded image, so zoom out a bit using your browser for a better overview)

![./spec/demo/demo_scroll (2Mb gif)](./spec/demo/demo_scroll.gif | width=200)

![screenshot](./spec/demo/demo_01.png | width=200)![screenshot](./spec/demo/demo_02.png | width=200)

![screenshot](./spec/demo/demo_03.png | width=200)![screenshot](./spec/demo/demo_04.png | width=200)

## Tools & Frameworks
Networking : RetroFit 2, Picasso

Dependency Injection : Dagger 2

Observable : Rx 2

Test : JUnit, Mockito, Espresso

Convenience Utils:

Logging : Timber

View : Butter Knife

SVG editor : Inkscape

bitmap editor : GIMP

## Architecture
ModelViewPresenter with contract interface

see also 
>    BaseMVPActivity �Compo, View, Presenter �

- Data model are abstracted at each level.
    - Tweet(data model from Twitter skd) ->  DisplayableItem (for UI)

## Rotation
The app work on both orientation, but the data is not reloaded after rotation.

## UI design
The design is extremly simple. Because I did not have template, I did not spend much time with margin and color.

### UI Assets
as much as possible I use emoji or SVG. This remove the resolution issue and allow the use of "tint".

### Assets Source
I made the custom design for this app in SVG. The images source are avaible in ./design/source

### API limitations
G+ return different post with the same image using a different image URL making it impossible to remove duplicates.

## Test
I made a test of each kind for demo purposes. 

- All test run without internet connection
- OfflineTestRunner inject the AppOfflineComponent into the Espresso application for offline testing
- SearchActivityEspressoTest (run on a device)
- SearchPresenterTest with Junit (run without a device)

