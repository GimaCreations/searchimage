package com.guilmart.imagesearch.core.base;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.ViewAssertion;
import android.support.test.espresso.ViewInteraction;
import android.view.View;

import com.guilmart.imagesearch.MyApplication;
import com.guilmart.imagesearch.core.injection.base.BaseAppComponent;
import com.guilmart.imagesearch.core.utils.RecyclerViewMatcher;

import org.hamcrest.Matcher;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.not;

/**
 * Created by Ch.A.Guilmart.
 */

public class BaseEspressoTest {

    protected ViewInteraction findView(@IdRes int viewIdToFound) {
        return onView(withId(viewIdToFound));
    }

    protected ViewAssertion isVisible() {
        return matches(isDisplayed());
    }

    protected ViewAssertion isNOTVisible() {
        return matches(not(isDisplayed()));
    }

    private Matcher<View> alwaysMatch() {
        return isAssignableFrom(View.class);
    }

    /**
     * Useful to wait for some animation. avoid using this as much as possible.
     */
    protected ViewAction waitFor(final long millis) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return alwaysMatch();
            }

            @Override
            public String getDescription() {
                return "Wait for " + millis + " milliseconds.";
            }

            @Override
            public void perform(UiController uiController, final View view) {
                uiController.loopMainThreadForAtLeast(millis);
            }
        };
    }

    protected RecyclerViewMatcher withRecyclerView(final int recyclerViewId) {
        return new RecyclerViewMatcher(recyclerViewId);
    }

    protected Context getAppContext() {
        return InstrumentationRegistry.getTargetContext();
    }

    protected BaseAppComponent getAppComponent() {
        return MyApplication.getComponentWithContext(getAppContext());
    }
}

