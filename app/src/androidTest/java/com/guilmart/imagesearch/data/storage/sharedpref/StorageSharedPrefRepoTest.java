package com.guilmart.imagesearch.data.storage.sharedpref;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by Ch.A.Guilmart.
 */
@RunWith(AndroidJUnit4.class)
public class StorageSharedPrefRepoTest {

    private static StorageSharedPrefRepo getStorageSharedPrefRepo() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        StorageSharedPrefRepo storageSharedPrefRepo = new StorageSharedPrefRepo(appContext);
        return storageSharedPrefRepo;
    }

    @Test
    public void saveAndReadTest() {
        StorageSharedPrefRepo storageSharedPrefRepo = getStorageSharedPrefRepo();

        String key1 = "key1";
        String value1 = "value1";
        String key2 = "key2";
        String value2 = "value2";
        String observed;

        storageSharedPrefRepo.clear();

        observed = storageSharedPrefRepo.get(key1);
        Assert.assertNull("key is not in use", observed);

        storageSharedPrefRepo.put(key1, value1);
        observed = storageSharedPrefRepo.get(key1);
        Assert.assertEquals("write-read test", value1, observed);

        // other key

        observed = storageSharedPrefRepo.get(key2);
        Assert.assertNull("key is not in use", observed);

        storageSharedPrefRepo.put(key2, value2);
        observed = storageSharedPrefRepo.get(key2);
        Assert.assertEquals("write-read test", value2, observed);

        // first key not affected

        observed = storageSharedPrefRepo.get(key1);
        Assert.assertEquals("write-read persistent test", value1, observed);
    }

    @Test
    public void saveAndClearAndReadTest() {
        StorageSharedPrefRepo storageSharedPrefRepo = getStorageSharedPrefRepo();

        String key1 = "key1";
        String value1 = "value1";
        String observed;

        storageSharedPrefRepo.clear();

        observed = storageSharedPrefRepo.get(key1);
        Assert.assertNull("key is not in use", observed);

        storageSharedPrefRepo.put(key1, value1);
        observed = storageSharedPrefRepo.get(key1);
        Assert.assertEquals("write-read test", value1, observed);

        storageSharedPrefRepo.clear();

        observed = storageSharedPrefRepo.get(key1);
        Assert.assertNull("key was erased", observed);
    }
}
