package com.guilmart.imagesearch.feature.global;

import android.app.Application;
import android.content.Context;
import android.support.test.runner.AndroidJUnitRunner;

import com.guilmart.imagesearch.MyApplication;
import com.guilmart.imagesearch.core.injection.module.ContextModule;

/**
 * Created by Ch.A.Guilmart.
 */

public class OfflineTestRunner extends AndroidJUnitRunner {

    @Override
    public Application newApplication(ClassLoader cl, String className, Context context) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        Application application = super.newApplication(cl, className, context);

        DaggerAppOfflineComponent.Builder builder = DaggerAppOfflineComponent.builder()
                .contextModule(new ContextModule(application));
        AppOfflineComponent component = builder.build();

        ((MyApplication) application).setComponent(component);

        return application;
    }
}
