package com.guilmart.imagesearch.feature.search;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.SearchView;

import com.guilmart.imagesearch.R;
import com.guilmart.imagesearch.core.base.BaseEspressoTest;
import com.guilmart.imagesearch.data.socialmedia.SocialMediaTestRepoProvider;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.hasFocus;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by Ch.A.Guilmart.
 */
@RunWith(AndroidJUnit4.class)
public class SearchActivityEspressoTest extends BaseEspressoTest {

    @Rule
    public ActivityTestRule<SearchActivity> activityRule =
            new ActivityTestRule<>(SearchActivity.class);

    // Reminder : look in the base class for findView() and friends

    @Test
    public void clickFabTest() {
        int recyclerview = R.id.recyclerview;

        findView(R.id.fab)
                .check(isVisible());

        findView(recyclerview)
                .check(isVisible());

        // clicking the FAB should move the focus to the search view
        findView(R.id.fab)
                .perform(click())
                .check(isVisible());
        onView(isAssignableFrom(SearchView.class))
                .check(matches(hasFocus()));
    }

    /**
     * against SocialMediaFakeRepo
     */
    @Test
    public void searchTest() {

        int recyclerview = R.id.recyclerview;
        String TEST_QUERY = "test query";

        //
        // do the query
        //

        // type in the search view
        onView(isAssignableFrom(SearchView.class))
                .perform(click())
                .check(isVisible());

        //onView(isAssignableFrom(SearchView.class)) // this don't work because Espresso cannot type inside support.SearchView
        findView(android.support.design.R.id.search_src_text) // the searchview input text View.
                .perform(typeText(TEST_QUERY));

        findView(R.id.toolbar_search)
                .perform(click());

        for (int index = 0; index < 2; index++) {
            String expectedFirstItemMainText = SocialMediaTestRepoProvider.SocialMediaFakeRepo.previousResult.get(index).getMainText();

            // the result is _somewhere_ on screen
            onView(withText(expectedFirstItemMainText))
                    .check(matches(isDisplayed()));

            // the list contain an item at this _index_ position
            onView(withRecyclerView(recyclerview)
                    .atPosition(index))
                    .check(matches(isDisplayed()));
            onView(withRecyclerView(recyclerview)
                    .atPosition(index))
                    .check(matches(hasDescendant(withId(R.id.item_main_text))));

            // this item contain the expected value for this index.
            onView(withRecyclerView(recyclerview)
                    .atPosition(index))
                    .check(matches(hasDescendant(withText(expectedFirstItemMainText))));
        }
    }

}
