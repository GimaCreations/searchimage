package com.guilmart.imagesearch.core.base.view;

import android.content.Context;
import android.os.Bundle;

import com.guilmart.imagesearch.MyApplication;
import com.guilmart.imagesearch.core.base.presenter.BasePresenter;
import com.guilmart.imagesearch.core.injection.base.BaseAppComponent;
import com.guilmart.imagesearch.core.injection.withComponent;

import javax.inject.Inject;

import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * base class with support for dependency injection and the MVP model
 * <p>
 * Requirement:
 * child class MUST implements <VIEW>
 * ComponentInterface MUST provide Presenter
 * <p>
 * Created by Ch.A.Guilmart.
 */

public abstract class BaseMVPActivity
        <ComponentInterface,
                VIEW extends BaseView,
                Presenter extends BasePresenter<VIEW>>
        extends BaseActivity
        implements withComponent<ComponentInterface>, BaseView {

    protected ComponentInterface component;
    @Inject
    protected Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (component == null) {
            component = createComponent();
        }
        inject(component);

        //ButterKnife.setDebug(true); //TODO : remove
        ButterKnife.bind(this);
        setupView();
        presenter.attachView((VIEW) this);
    }

    /**
     * after ButterKnife Binding, Prior to Presenter attachView
     */
    protected abstract void setupView();

    @Override
    protected void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }

    @Override
    public Context context() {
        return this;
    }

    @Override
    public ComponentInterface getComponent() {
        return component;
    }

    @Override
    public void setComponent(ComponentInterface component) {
        this.component = component;
    }

    protected BaseAppComponent getAppComponent() {
        return MyApplication.getComponentWithContext(this);
    }

    @Override
    public void showError(String message) {
        Timber.e("showError %s", message);
    }

    @Override
    public void showLoading() {
    }

    @Override
    public void hideLoading() {
    }

}
