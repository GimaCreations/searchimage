package com.guilmart.imagesearch.core.base.view;

import android.content.Context;

/**
 * Base Interface for class (typically activity or fragment)
 * acting as VIEW from the MVP model.
 * <p>
 * Created by Ch.A.Guilmart.
 */
public interface BaseView {
    void showLoading();

    void hideLoading();

    void showError(String message);

    /**
     * NOT the application context.
     */
    Context context();
}
