package com.guilmart.imagesearch.core.injection.module;

import android.content.Context;

import com.guilmart.imagesearch.core.injection.qualifier.ApplicationContextQualifier;
import com.guilmart.imagesearch.core.injection.scope.PerApplication;
import com.guilmart.imagesearch.data.storage.StorageRepo;
import com.guilmart.imagesearch.data.storage.sharedpref.StorageSharedPrefRepo;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Ch.A.Guilmart.
 */

@Module(includes = ContextModule.class)
public class StorageModule {

    @Provides
    @PerApplication
    StorageRepo provideStorageRepo(@ApplicationContextQualifier Context context) {
        return new StorageSharedPrefRepo(context);
    }
}
