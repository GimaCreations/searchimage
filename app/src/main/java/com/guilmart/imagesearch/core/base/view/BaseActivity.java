package com.guilmart.imagesearch.core.base.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Base class. Mostly meant to hold convenience function.
 * <p>
 * Created by Ch.A.Guilmart.
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentViewResId());
    }

    @LayoutRes
    protected abstract int getContentViewResId();

    protected void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {

        }
    }

    protected View getRootView() {
        View content = findViewById(android.R.id.content);
        if (content != null) {
            return content;
        }
        View root = getWindow().getDecorView().getRootView();
        return root;
    }
}
