package com.guilmart.imagesearch.core.injection.module;

import com.guilmart.imagesearch.core.injection.scope.PerApplication;
import com.guilmart.imagesearch.data.history.HistoryRepo;
import com.guilmart.imagesearch.data.history.HistoryStorageRepo;
import com.guilmart.imagesearch.data.storage.StorageRepo;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Ch.A.Guilmart.
 */

@Module(includes = StorageModule.class)
public class HistoryRepoModule {

    @Provides
    @PerApplication
    HistoryRepo provideHistoryRepo(StorageRepo storageRepo) {
        return new HistoryStorageRepo(storageRepo);
    }
}
