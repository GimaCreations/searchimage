package com.guilmart.imagesearch.core.injection;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by Ch.A.Guilmart.
 */

public interface withComponent<ComponentInterface> {
    /**
     * it MUST be called after getComponentWithContext()/createComponent()
     */
    void inject(@NonNull ComponentInterface component);

    /**
     * it MUST NOT be called if getComponentWithContext() return an instance.
     * (This allow to inject mock component without code change).
     */
    @NonNull
    ComponentInterface createComponent();

    @Nullable
    ComponentInterface getComponent();

    void setComponent(ComponentInterface component);
}
