package com.guilmart.imagesearch.core.utils;

import android.os.Build;

import java.util.Locale;

/**
 * Created by Ch.A.Guilmart.
 */

public final class LocaleUtils {
    // static class
    private LocaleUtils() {
    }

    public static Locale getUserLocale() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Locale.getDefault(Locale.Category.DISPLAY);
        }
        return Locale.getDefault();
    }

    public static String getUserLocaleAsString() {
        Locale locale = getUserLocale();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return locale.toLanguageTag();
        }
        return locale.toString();
    }

}
