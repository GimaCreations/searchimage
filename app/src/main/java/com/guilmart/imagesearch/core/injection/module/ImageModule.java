package com.guilmart.imagesearch.core.injection.module;

import android.content.Context;

import com.guilmart.imagesearch.BuildConfig;
import com.guilmart.imagesearch.core.injection.qualifier.ApplicationContextQualifier;
import com.guilmart.imagesearch.core.injection.scope.PerApplication;
import com.guilmart.imagesearch.feature.imageloader.ImageLoader;
import com.guilmart.imagesearch.feature.imageloader.ImageLoaderProduction;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

/**
 * Images Download and manipulation
 * <p>
 * Created by Ch.A.Guilmart.
 */
@Module(includes = {ClientModule.class, ContextModule.class})
public class ImageModule {

    @Provides
    @PerApplication
    ImageLoader provideImageLoader(@ApplicationContextQualifier Context context, Picasso picasso) {
        return new ImageLoaderProduction(context, picasso);
    }

    @Provides
    @PerApplication
    Picasso providePicasso(@ApplicationContextQualifier Context context, OkHttpClient okHttpClient) {
        Picasso.Builder builder = new Picasso.Builder(context);
        // picasso:<2.6 don't support okhttp3 --> use OkHttp3Downloader
        builder.downloader(new OkHttp3Downloader(okHttpClient));
        Picasso picasso = builder.build();
        picasso.setIndicatorsEnabled(BuildConfig.DEBUG);
        return picasso;
    }

}
