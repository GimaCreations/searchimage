package com.guilmart.imagesearch.core.base.presenter;

import com.guilmart.imagesearch.core.base.view.BaseView;

/**
 * Base Interface for class
 * acting as PRESENTER from the MVP model.
 * <p>
 * Created by Ch.A.Guilmart.
 */

public interface BasePresenter<V extends BaseView> {
    void attachView(V view);

    void detachView();
}
