package com.guilmart.imagesearch.core.base.view;

import android.support.annotation.NonNull;

/**
 * Created by Ch.A.Guilmart.
 */
public interface DisplayableItem {
    String getMainText();

    String getSecondaryText();

    /**
     * @return empty ("") if there is no image
     */
    @NonNull
    String getImageURL();
}
