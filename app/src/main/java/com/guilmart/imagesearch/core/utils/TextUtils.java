package com.guilmart.imagesearch.core.utils;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by Ch.A.Guilmart.
 */

public class TextUtils {

    public static boolean isEmpty(String s) {
        return android.text.TextUtils.isEmpty(s);
    }

    @NonNull
    public static String getQueryParameterFromUri(@Nullable String uri, String name) {
        return Uri.parse(uri).getQueryParameter(name);
    }
}
