package com.guilmart.imagesearch.core.injection.base;


import android.content.Context;

import com.guilmart.imagesearch.MyApplication;
import com.guilmart.imagesearch.core.injection.qualifier.ApplicationContextQualifier;
import com.guilmart.imagesearch.data.history.HistoryRepo;
import com.guilmart.imagesearch.data.socialmedia.SocialMediaRepo;
import com.guilmart.imagesearch.feature.imageloader.ImageLoader;
import com.guilmart.imagesearch.feature.navigator.Navigator;

/**
 * Created by Ch.A.Guilmart.
 */

public interface BaseAppComponent {
    void injectMyApplication(MyApplication myApplication);

    //
    // Publish some stuff, so component which depend on it can use them
    //

    @ApplicationContextQualifier
    Context context();

    ImageLoader imageLoader();

    SocialMediaRepo socialMediaRepo();

    HistoryRepo historyRepo();

    Navigator navigator();
}
