package com.guilmart.imagesearch.core.injection.module;

import android.content.Context;

import com.guilmart.imagesearch.core.injection.qualifier.ApplicationContextQualifier;
import com.guilmart.imagesearch.core.injection.scope.PerApplication;

import dagger.Module;
import dagger.Provides;

/**
 * Used for Application context.
 * <p>
 * Created by Ch.A.Guilmart.
 */
@Module
public class ContextModule {

    private final Context context;

    public ContextModule(Context context) {
        Context valueToSet = context;
        if (context.getApplicationContext() != null) {
            // it seem that AndroidJUnitRunner application.getApplicationContext() return null.
            valueToSet = context.getApplicationContext();
        }
        this.context = valueToSet;
    }

    @Provides
    @PerApplication
    @ApplicationContextQualifier
    public Context provideContext() {
        return context;
    }

}
