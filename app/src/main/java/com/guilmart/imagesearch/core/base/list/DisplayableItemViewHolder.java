package com.guilmart.imagesearch.core.base.list;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.guilmart.imagesearch.core.base.view.DisplayableItem;

import butterknife.ButterKnife;

/**
 * Created by Ch.A.Guilmart.
 */

public abstract class DisplayableItemViewHolder extends RecyclerView.ViewHolder {

    public DisplayableItemViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public abstract void load(DisplayableItem item);
}
