package com.guilmart.imagesearch.core.base.list;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.guilmart.imagesearch.core.base.presenter.BasePresenter;
import com.guilmart.imagesearch.core.base.view.DisplayableItem;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * main VIEW responsible to display the list of Displayable.
 * <p>
 * Created by Ch.A.Guilmart.
 */

public abstract class ListBaseAdapter
        <P extends BasePresenter, VH extends DisplayableItemViewHolder>
        extends RecyclerView.Adapter<VH> {

    @NonNull
    protected final List<DisplayableItem> list = new ArrayList<>();

    @Inject
    protected P presenter;

    public void appendToList(List<DisplayableItem> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void appendToList(DisplayableItem item) {
        this.list.add(item);
        notifyDataSetChanged();
    }

    public void clear() {
        this.list.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    protected View inflate(ViewGroup parent, @LayoutRes int layoutId) {
        return LayoutInflater.from(parent.getContext())
                .inflate(layoutId, parent, false);
    }

}