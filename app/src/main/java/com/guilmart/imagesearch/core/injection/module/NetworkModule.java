package com.guilmart.imagesearch.core.injection.module;

import android.content.Context;

import com.guilmart.imagesearch.BuildConfig;
import com.guilmart.imagesearch.core.injection.qualifier.ApplicationContextQualifier;
import com.guilmart.imagesearch.core.injection.scope.PerApplication;

import java.io.File;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by Ch.A.Guilmart.
 */

@Module(includes = ContextModule.class)

public class NetworkModule {

    private static final int CACHE_MAX_SIZE = 10 * /* Mb */ 1024 * 1024;

    @Provides
    @PerApplication
    OkHttpClient.Builder provideOkHttpClientBuilder(Cache cache, HttpLoggingInterceptor logger) {
        return new OkHttpClient.Builder()
                .addInterceptor(logger)
                .cache(cache);
    }

    @Provides
    @PerApplication
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor logger = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            logger.setLevel(HttpLoggingInterceptor.Level.BASIC);
        }
        return logger;
    }

    @Provides
    @PerApplication
    Cache provideCache(File cacheFile) {
        return new Cache(cacheFile, CACHE_MAX_SIZE);
    }

    @Provides
    @PerApplication
    File provideCacheFile(@ApplicationContextQualifier Context context) {
        return new File(context.getCacheDir(), "network_cache");
    }

}
