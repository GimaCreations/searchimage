package com.guilmart.imagesearch.core.base.view;

import android.support.annotation.NonNull;

/**
 * Created by Ch.A.Guilmart.
 */

public class DisplayableItemImpl implements DisplayableItem {

    private String mainText;
    private String imageUrl;

    public DisplayableItemImpl(String mainText, String imageUrl) {
        this.mainText = mainText;
        this.imageUrl = imageUrl;
    }

    public DisplayableItemImpl() {
        this("", "");
    }

    @Override
    public String getMainText() {
        return mainText;
    }

    @Override
    public String getSecondaryText() {
        return "";
    }

    @NonNull
    @Override
    public String getImageURL() {
        return imageUrl;
    }

}
