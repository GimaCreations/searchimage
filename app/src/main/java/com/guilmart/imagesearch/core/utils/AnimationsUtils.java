package com.guilmart.imagesearch.core.utils;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import java.util.Set;

/**
 * Collection of animations
 * <p>
 * Created by Ch.A.Guilmart.
 */

public class AnimationsUtils {
    @NonNull
    private final static Set<View> animating = new android.support.v4.util.ArraySet<>();

    // static class
    private AnimationsUtils() {
    }

    private static void startAnimating(View v) {
        animating.add(v);
    }

    private static void finishAnimating(View v) {
        animating.remove(v);
    }

    private static boolean isAnimating(View v) {
        return animating.contains(v);
    }

    /**
     * given a view : show it, shake it then apply endVisibility
     */
    public static void tease(@NonNull View v, int endVisibility) {
        if (isAnimating(v)) {
            return;
        }
        long duration = 500; //total duration is 3.5x this value

        TimeInterpolator interpolator = new AccelerateDecelerateInterpolator();

        ObjectAnimator rotationRepeat = ObjectAnimator
                .ofFloat(v, "rotation", -10f, 10f)
                .setDuration(duration);
        rotationRepeat.setRepeatCount(3);
        rotationRepeat.setRepeatMode(ValueAnimator.REVERSE);
        rotationRepeat.setInterpolator(interpolator);

        ObjectAnimator rotationFinish = ObjectAnimator
                .ofFloat(v, "rotation", -10f, 0f)
                .setDuration(duration / 2);
        rotationRepeat.setInterpolator(interpolator);

        ObjectAnimator scaleYRepeat = ObjectAnimator
                .ofFloat(v, "scaleY", 0.8f, 1.2f)
                .setDuration(duration);
        scaleYRepeat.setRepeatCount(3);
        scaleYRepeat.setRepeatMode(ValueAnimator.REVERSE);
        scaleYRepeat.setInterpolator(interpolator);

        ObjectAnimator scaleYFinish = ObjectAnimator
                .ofFloat(v, "scaleY", 0.8f, 1f)
                .setDuration(duration / 2);
        scaleYRepeat.setInterpolator(interpolator);

        ObjectAnimator scaleXRepeat = ObjectAnimator
                .ofFloat(v, "scaleX", 0.8f, 1.2f)
                .setDuration(duration);
        scaleXRepeat.setRepeatCount(3);
        scaleXRepeat.setRepeatMode(ValueAnimator.REVERSE);
        scaleXRepeat.setInterpolator(interpolator);

        ObjectAnimator scaleXFinish = ObjectAnimator
                .ofFloat(v, "scaleX", 0.8f, 1f)
                .setDuration(duration / 2);
        scaleXRepeat.setInterpolator(interpolator);

        // FINISH
        Animator.AnimatorListener finishListener = new AnimatorListenerSetVisibility(v, endVisibility);

        AnimatorSet animationFinish = new AnimatorSet();
        animationFinish.addListener(finishListener);
        animationFinish.playTogether(rotationFinish, scaleYFinish, scaleXFinish);

        // REPEAT
        Animator.AnimatorListener repeatListener = new AnimatorListenerSetVisibility(v, View.VISIBLE) {
            @Override
            public void onAnimationEnd(Animator animator) {
                super.onAnimationEnd(animator);
                animationFinish.start();
            }
        };
        AnimatorSet animationRepeat = new AnimatorSet();
        animationRepeat.playTogether(rotationRepeat, scaleYRepeat, scaleXRepeat);
        animationRepeat.addListener(repeatListener);

        // DO IT
        animationRepeat.start();
    }

    private static class AnimatorListenerSetVisibility implements Animator.AnimatorListener {
        protected View v;
        protected int endVisibility;

        AnimatorListenerSetVisibility(@NonNull View v, int endVisibility) {
            this.v = v;
            this.endVisibility = endVisibility;
        }

        @Override
        public void onAnimationStart(Animator animator) {
            v.setVisibility(View.VISIBLE);
            startAnimating(v);
        }

        @Override
        public void onAnimationEnd(Animator animator) {
            v.setVisibility(endVisibility);
            finishAnimating(v);
        }

        @Override
        public void onAnimationCancel(Animator animator) {
            v.setVisibility(endVisibility);
            finishAnimating(v);
        }

        @Override
        public void onAnimationRepeat(Animator animator) {
        }

    }

}
