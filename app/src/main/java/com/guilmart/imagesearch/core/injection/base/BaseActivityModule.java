package com.guilmart.imagesearch.core.injection.base;

import com.guilmart.imagesearch.core.base.view.BaseActivity;
import com.guilmart.imagesearch.core.injection.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Ch.A.Guilmart.
 */

@Module
public class BaseActivityModule {

    protected final BaseAppComponent appComponent;
    protected final BaseActivity activity;

    public BaseActivityModule(BaseAppComponent appComponent, BaseActivity activity) {
        this.appComponent = appComponent;
        this.activity = activity;
    }

    @Provides
    @PerActivity
    protected BaseActivity activity() {
        return this.activity;
    }
}
