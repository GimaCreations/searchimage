package com.guilmart.imagesearch.core.base.presenter;

import android.content.Context;

import com.guilmart.imagesearch.core.base.view.BaseView;

/**
 * Created by Ch.A.Guilmart.
 */

public class BasePresenterImp<V extends BaseView> implements BasePresenter<V> {

    private V view;

    @Override
    public void attachView(V view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    protected boolean hasView() {
        return this.view != null;
    }

    protected V getView() {
        return view;
    }

    protected final Context getContext() {
        return getView().context();
    }
}
