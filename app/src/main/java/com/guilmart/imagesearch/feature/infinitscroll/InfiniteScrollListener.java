package com.guilmart.imagesearch.feature.infinitscroll;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import timber.log.Timber;

/**
 * Infinite Scroll Listener with pagination support
 * <p>
 * Created by Ch.A.Guilmart.
 */

public class InfiniteScrollListener extends RecyclerView.OnScrollListener {
    /**
     * when the number of item after the last visible one is smaller than that, we load more data.
     */
    private static final int MIN_BOTTOM_ITEM_COUNT = 5;// MUST be inferior to the number of item per page !

    final private PresenterWithPager fetcher;

    /**
     * {@link #getItemCount} value during previous call to {@link #onScrollStateChanged}
     */
    private int currentItemCount;
    /**
     * is TRUE while we wait for more data to arrive.
     */
    private boolean loading;

    public InfiniteScrollListener(PresenterWithPager dataFetcher) {
        this.fetcher = dataFetcher;
        reset();
    }

    private void reset() {
        this.currentItemCount = 0;
        this.loading = false;
    }

    private int findLastVisibleItemPositions(@NonNull RecyclerView view) {
        RecyclerView.LayoutManager lm = view.getLayoutManager();
        if (lm instanceof LinearLayoutManager) {
            LinearLayoutManager llm = (LinearLayoutManager) lm;
            return llm.findLastVisibleItemPosition();
        } else {
            if (lm == null) {
                Timber.e("findLastVisibleItemPositions : LayoutManager is null");
            } else {
                //TODO add support for other class
                Timber.e("findLastVisibleItemPositions : LayoutManager's Class unsupported: %s", lm.getClass().getSimpleName());
            }
        }
        return -1;
    }

    private int getItemCount(@NonNull RecyclerView view) {
        return view.getLayoutManager().getItemCount();
    }

    /**
     * change internal state and fetch
     */
    private void fetchNextPage() {
        loading = fetcher.fetchNextPage();
        //TODO : when this return false: this is no more page -> enter a final state which stop fetching pages / make reset public.
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        // I do not use onScrolled() as you may found in many example. It is costly.
        // I use onScrollStateChanged because it is called much less often.

        if (newState == RecyclerView.SCROLL_STATE_IDLE) {

            if (recyclerView == null) {
                // we cannot do a thing without a view
                return;
            }

            //
            // get value and validate them
            //
            int lastVisibleItemPosition = findLastVisibleItemPositions(recyclerView);
            if (lastVisibleItemPosition < 0) {
                // should never happens OR wrong type of LayoutManager
                return;
            }
            //Timber.d("onScrollStateChanged. lastVisibleItemPosition:" + lastVisibleItemPosition);
            int newItemCount = getItemCount(recyclerView);

            if (newItemCount < currentItemCount) {
                // something erase the list --> reset the state.
                reset();
            }

            //
            // finish to load ?
            //
            // assumption : when we finished to load, we got more items.
            // This is a safety / fallback. This case is managed by onNewData()
            if (loading && newItemCount > currentItemCount) {
                loading = false;
            }
            // update the value for next onScrolled() call
            currentItemCount = newItemCount;

            //
            // need to load more ?
            //
            if (!loading && (lastVisibleItemPosition + MIN_BOTTOM_ITEM_COUNT >= newItemCount)) {
                //Timber.d(" LOAD MORE %d / %d ", lastVisibleItemPosition, newItemCount);
                fetchNextPage();
            }
        }
    }

    /**
     * call this when the next page finish to load.
     * Meaning the listen can fetch more page.
     */
    public void onNewData() {
        loading = false;
    }
}
