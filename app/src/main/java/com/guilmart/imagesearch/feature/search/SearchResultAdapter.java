package com.guilmart.imagesearch.feature.search;

import android.view.View;
import android.view.ViewGroup;

import com.guilmart.imagesearch.R;
import com.guilmart.imagesearch.core.base.list.ListBaseAdapter;
import com.guilmart.imagesearch.core.base.view.DisplayableItem;
import com.guilmart.imagesearch.feature.imageloader.ImageLoader;

import javax.inject.Inject;

/**
 * Created by Ch.A.Guilmart.
 */

public class SearchResultAdapter
        extends ListBaseAdapter<SearchContract.Presenter, SearchResultViewHolder> {

    @Inject
    protected ImageLoader imageLoader;

    @Inject
    public SearchResultAdapter() {
    }

    @Override
    public SearchResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflate(parent, R.layout.search_result_item);
        return new SearchResultViewHolder(view, imageLoader);
    }

    @Override
    public void onBindViewHolder(SearchResultViewHolder holder, int position) {
        final DisplayableItem item = list.get(position);
        holder.load(item);
        holder.itemView.setOnClickListener(v -> presenter.navigateToDetail(item));
    }
}
