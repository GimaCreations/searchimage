package com.guilmart.imagesearch.feature.history;

import com.guilmart.imagesearch.core.base.presenter.BasePresenterImp;
import com.guilmart.imagesearch.core.base.view.DisplayableItem;
import com.guilmart.imagesearch.data.history.HistoryRepo;
import com.guilmart.imagesearch.data.history.SearchQuery;
import com.guilmart.imagesearch.feature.navigator.Navigator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ch.A.Guilmart.
 */

public class HistoryPresenter
        extends BasePresenterImp<HistoryContract.VIEW>
        implements HistoryContract.Presenter {

    private final HistoryRepo historyRepo;
    private final Navigator navigator;

    public HistoryPresenter(HistoryRepo historyRepo, Navigator navigator) {
        this.historyRepo = historyRepo;
        this.navigator = navigator;
    }

    @Override
    public void clearHistory() {
        historyRepo.clear();
    }

    @Override
    public void loadHistory() {
        List<SearchQuery> list = historyRepo.getHistory();
        List<DisplayableItem> ret = new ArrayList<>();
        for (SearchQuery s : list) {
            ret.add(new HistoryItem(s));
        }
        getView().populate(ret);
    }

    @Override
    public void searchAgain(DisplayableItem item) {
        String query = "";
        if (item instanceof HistoryItem) {
            query = ((HistoryItem) item).getQuery();
        }
        navigator.search(getContext(), query);
    }
}
