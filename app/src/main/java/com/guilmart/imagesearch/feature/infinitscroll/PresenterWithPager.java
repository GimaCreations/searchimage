package com.guilmart.imagesearch.feature.infinitscroll;

/**
 * Created by Ch.A.Guilmart.
 */

public interface PresenterWithPager {
    /**
     * query the next page
     *
     * @return true if it did the call. False if it was aborted (eg: because there is no more pages)
     */
    boolean fetchNextPage();

    /**
     * if this return False, then fetchNextPage will return false as well.
     */
    boolean hasNextPage();
}
