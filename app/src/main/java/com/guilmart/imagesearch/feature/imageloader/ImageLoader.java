package com.guilmart.imagesearch.feature.imageloader;

import android.support.annotation.NonNull;
import android.widget.ImageView;

/**
 * Proxy for Picasso
 * <p>
 * Created by Ch.A.Guilmart.
 */

public interface ImageLoader {
    void load(@NonNull ImageView iv, @NonNull String imageUrl);
}
