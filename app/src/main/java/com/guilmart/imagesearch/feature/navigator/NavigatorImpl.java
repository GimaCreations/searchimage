package com.guilmart.imagesearch.feature.navigator;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.guilmart.imagesearch.core.utils.TextUtils;
import com.guilmart.imagesearch.feature.history.HistoryActivity;
import com.guilmart.imagesearch.feature.search.SearchActivity;

/**
 * Created by Ch.A.Guilmart.
 */

public class NavigatorImpl implements Navigator {

    @Override
    public void history(@NonNull Context fromContext) {
        Intent intent = new Intent(fromContext, HistoryActivity.class);
        Intent[] intents = {intent};
        fromContext.startActivities(intents);
    }

    @Override
    public void search(@NonNull Context fromContext, @Nullable String query) {
        Intent intent = SearchActivity.getIntent(fromContext, query);
        Intent[] intents = {intent};
        fromContext.startActivities(intents);
    }

    @Override
    public void openUrl(@NonNull Context fromContext, @Nullable String url) {
        if (!TextUtils.isEmpty(url)) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            Intent[] intents = {intent};
            fromContext.startActivities(intents);
        }
    }

}
