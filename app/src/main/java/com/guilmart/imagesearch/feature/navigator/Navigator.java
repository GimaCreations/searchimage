package com.guilmart.imagesearch.feature.navigator;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Navigate within the app and the system (aka Intent provider)
 * <p>
 * Created by Ch.A.Guilmart.
 */

public interface Navigator {
    void history(@NonNull Context fromContext);

    void search(@NonNull Context fromContext, @Nullable String query);

    void openUrl(@NonNull Context fromContext, @NonNull String url);
}
