package com.guilmart.imagesearch.feature.navigator;

import android.support.annotation.Nullable;

/**
 * Created by Ch.A.Guilmart.
 */

public interface OpenableItem {
    @Nullable
    String getOpenUrl();
}
