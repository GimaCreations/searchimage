package com.guilmart.imagesearch.feature.search;


import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.guilmart.imagesearch.R;
import com.guilmart.imagesearch.core.base.list.DisplayableItemViewHolder;
import com.guilmart.imagesearch.core.base.view.DisplayableItem;
import com.guilmart.imagesearch.feature.imageloader.ImageLoader;

import butterknife.BindView;

/**
 * Created by Ch.A.Guilmart.
 */

public class SearchResultViewHolder extends DisplayableItemViewHolder {

    @BindView(R.id.item_main_image)
    ImageView mainImage;
    @BindView(R.id.item_main_text)
    TextView mainText;

    ImageLoader imageLoader;

    public SearchResultViewHolder(View itemView, ImageLoader imageLoader) {
        super(itemView);
        this.imageLoader = imageLoader;
    }

    @Override
    public void load(DisplayableItem item) {
        mainText.setText(item.getMainText());
        imageLoader.load(mainImage, item.getImageURL());
    }
}
