package com.guilmart.imagesearch.feature.history;

import com.guilmart.imagesearch.core.base.presenter.BasePresenter;
import com.guilmart.imagesearch.core.base.view.BaseView;
import com.guilmart.imagesearch.core.base.view.DisplayableItem;

import java.util.List;

/**
 * Created by Ch.A.Guilmart.
 */

public interface HistoryContract {

    interface VIEW extends BaseView {
        void populate(List<DisplayableItem> list);
    }

    interface Presenter extends BasePresenter<VIEW> {
        void clearHistory();

        void loadHistory();

        void searchAgain(DisplayableItem item);
    }
}
