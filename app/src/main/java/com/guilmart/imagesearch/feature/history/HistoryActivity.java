package com.guilmart.imagesearch.feature.history;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.guilmart.imagesearch.R;
import com.guilmart.imagesearch.core.base.view.BaseMVPActivity;
import com.guilmart.imagesearch.core.base.view.DisplayableItem;
import com.guilmart.imagesearch.feature.history.injection.DaggerHistoryComponent;
import com.guilmart.imagesearch.feature.history.injection.HistoryComponent;
import com.guilmart.imagesearch.feature.history.injection.HistoryModule;
import com.guilmart.imagesearch.feature.status.StatusUI;
import com.guilmart.imagesearch.feature.status.StatusUI.STATUS;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class HistoryActivity
        extends BaseMVPActivity<
        HistoryComponent,
        HistoryContract.VIEW,
        HistoryContract.Presenter>
        implements HistoryContract.VIEW {

    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    /**
     * inflated with the menu, set in onCreateOptionsMenu
     */
    MenuItem clearMenu;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @Inject
    HistoryAdapter historyAdapter;
    StatusUI statusUI;

    @Override
    protected int getContentViewResId() {
        return R.layout.activity_history;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.loadHistory();
    }

    @Override
    protected void setupView() {
        //
        // Status
        //
        statusUI = new StatusUI(getRootView()) {
            @Override
            protected int getText(STATUS status) {
                if (status == STATUS.NO_DATA) {
                    return R.string.history_no_data_message;
                }
                return super.getText(status);
            }
        };
        statusUI.display(STATUS.NO_DATA);

        //
        // Action Bar / Toolbar
        //
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
//        actionBar.setDisplayUseLogoEnabled(true);
//        actionBar.setLogo(R.drawable.);
        actionBar.setTitle(R.string.history_title);

        //
        //  List
        //
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.HORIZONTAL));
        recyclerView.setAdapter(historyAdapter);

        //
        // fab
        //
        fab.setOnClickListener((View view) -> {
            clearHistory();
        });
    }

    private void clearHistory() {
        presenter.clearHistory();
        historyAdapter.clear();
        statusUI.display(STATUS.NO_DATA);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.action_bar_history, menu);

        clearMenu = menu.findItem(R.id.menu_item_clear);
        clearMenu.setOnMenuItemClickListener(menuItem -> {
            clearHistory();
            return true;
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void inject(@NonNull HistoryComponent component) {
        component.injectActivity(this);
    }

    @NonNull
    @Override
    public HistoryComponent createComponent() {
        return DaggerHistoryComponent.builder()
                .historyModule(new HistoryModule(getAppComponent(), this))
                .build();
    }

    private boolean gotData() {
        return historyAdapter.getItemCount() > 0;
    }

    @Override
    public void populate(List<DisplayableItem> list) {
        historyAdapter.clear();
        historyAdapter.appendToList(list);
        if (gotData()) {
            statusUI.display(STATUS.GOT_DATA);
        }
    }
}
