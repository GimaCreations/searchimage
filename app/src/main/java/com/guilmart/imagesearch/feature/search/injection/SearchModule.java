package com.guilmart.imagesearch.feature.search.injection;

import android.widget.ArrayAdapter;

import com.guilmart.imagesearch.core.base.view.BaseActivity;
import com.guilmart.imagesearch.core.injection.base.BaseActivityModule;
import com.guilmart.imagesearch.core.injection.base.BaseAppComponent;
import com.guilmart.imagesearch.core.injection.scope.PerActivity;
import com.guilmart.imagesearch.data.history.HistoryRepo;
import com.guilmart.imagesearch.data.socialmedia.SocialMediaRepo;
import com.guilmart.imagesearch.feature.imageloader.ImageLoader;
import com.guilmart.imagesearch.feature.infinitscroll.InfiniteScrollListener;
import com.guilmart.imagesearch.feature.navigator.Navigator;
import com.guilmart.imagesearch.feature.search.SearchContract;
import com.guilmart.imagesearch.feature.search.SearchPresenter;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Ch.A.Guilmart.
 */
@Module
@PerActivity
public class SearchModule extends BaseActivityModule {
    public SearchModule(BaseAppComponent appComponent, BaseActivity activity) {
        super(appComponent, activity);
    }

    @Provides
    @PerActivity
    SearchContract.Presenter providePresenter(SocialMediaRepo socialMediaRepo, HistoryRepo historyRepo, Navigator navigator) {
        return new SearchPresenter(socialMediaRepo, historyRepo, navigator);
    }

    @Provides
    @PerActivity
    InfiniteScrollListener provideInfiniteScrollListener(SearchContract.Presenter presenter) {
        return new InfiniteScrollListener(presenter);
    }

    @Provides
    @PerActivity
    SocialMediaRepo socialMediaRepo() {
        return appComponent.socialMediaRepo();
    }

    @Provides
    @PerActivity
    ImageLoader provideImageLoader() {
        return appComponent.imageLoader();
    }

    @Provides
    @PerActivity
    HistoryRepo provideHistoryRepo() {
        return appComponent.historyRepo();
    }

    @Provides
    @PerActivity
    Navigator provideNavigator() {
        return appComponent.navigator();
    }


    @Provides
    @PerActivity
    ArrayAdapter<String> provideAutoCompleteAdapter(BaseActivity activity) {
        return new ArrayAdapter<>(activity, android.R.layout.simple_list_item_1, new ArrayList<>());
    }

}
