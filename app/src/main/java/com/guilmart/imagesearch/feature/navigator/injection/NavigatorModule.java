package com.guilmart.imagesearch.feature.navigator.injection;

import com.guilmart.imagesearch.core.injection.scope.PerApplication;
import com.guilmart.imagesearch.feature.navigator.Navigator;
import com.guilmart.imagesearch.feature.navigator.NavigatorImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Ch.A.Guilmart.
 */

@Module
public class NavigatorModule {

    @Provides
    @PerApplication
    Navigator provideNavigator() {
        return new NavigatorImpl();
    }

}
