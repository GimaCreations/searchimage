package com.guilmart.imagesearch.feature.global;

import com.guilmart.imagesearch.core.injection.base.BaseAppComponent;
import com.guilmart.imagesearch.core.injection.module.HistoryRepoModule;
import com.guilmart.imagesearch.core.injection.module.ImageModule;
import com.guilmart.imagesearch.core.injection.scope.PerApplication;
import com.guilmart.imagesearch.data.socialmedia.injection.SocialMediaModule;
import com.guilmart.imagesearch.feature.navigator.injection.NavigatorModule;

import dagger.Component;

/**
 * Created by Ch.A.Guilmart.
 */

@PerApplication
@Component(modules = {ImageModule.class, SocialMediaModule.class, HistoryRepoModule.class, NavigatorModule.class})
public interface AppComponent extends BaseAppComponent {
}
