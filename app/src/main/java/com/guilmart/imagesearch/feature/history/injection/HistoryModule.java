package com.guilmart.imagesearch.feature.history.injection;

import com.guilmart.imagesearch.core.base.view.BaseActivity;
import com.guilmart.imagesearch.core.injection.base.BaseActivityModule;
import com.guilmart.imagesearch.core.injection.base.BaseAppComponent;
import com.guilmart.imagesearch.core.injection.scope.PerActivity;
import com.guilmart.imagesearch.data.history.HistoryRepo;
import com.guilmart.imagesearch.feature.history.HistoryContract;
import com.guilmart.imagesearch.feature.history.HistoryPresenter;
import com.guilmart.imagesearch.feature.navigator.Navigator;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Ch.A.Guilmart.
 */
@Module
public class HistoryModule extends BaseActivityModule {

    public HistoryModule(BaseAppComponent appComponent, BaseActivity activity) {
        super(appComponent, activity);
    }

    @Provides
    @PerActivity
    HistoryRepo provideHistoryRepo() {
        return appComponent.historyRepo();
    }

    @Provides
    @PerActivity
    Navigator provideNavigator() {
        return appComponent.navigator();
    }

    @Provides
    @PerActivity
    HistoryContract.Presenter provideHistoryContractPresenter(HistoryRepo historyRepo, Navigator navigator) {
        return new HistoryPresenter(historyRepo, navigator);
    }

}
