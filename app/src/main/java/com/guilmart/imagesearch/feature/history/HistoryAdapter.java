package com.guilmart.imagesearch.feature.history;

import android.view.View;
import android.view.ViewGroup;

import com.guilmart.imagesearch.R;
import com.guilmart.imagesearch.core.base.list.ListBaseAdapter;
import com.guilmart.imagesearch.core.base.view.DisplayableItem;

import javax.inject.Inject;

/**
 * Created by Ch.A.Guilmart.
 */

public class HistoryAdapter extends ListBaseAdapter<HistoryContract.Presenter, HistoryViewHolder> {
    @Inject
    public HistoryAdapter() {
    }

    @Override
    public HistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflate(parent, R.layout.history_item);
        HistoryViewHolder historyViewHolder = new HistoryViewHolder(v);
        return historyViewHolder;
    }

    @Override
    public void onBindViewHolder(HistoryViewHolder holder, int position) {
        DisplayableItem item = list.get(position);
        holder.load(item);
        holder.itemView.setOnClickListener(view -> {
            presenter.searchAgain(item);
        });
    }
}
