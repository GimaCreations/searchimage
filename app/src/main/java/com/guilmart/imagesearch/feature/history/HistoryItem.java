package com.guilmart.imagesearch.feature.history;

import com.guilmart.imagesearch.core.base.view.DisplayableItem;
import com.guilmart.imagesearch.core.utils.LocaleUtils;
import com.guilmart.imagesearch.data.history.SearchQuery;

import java.text.DateFormat;
import java.util.Date;


/**
 * Created by Ch.A.Guilmart.
 */
public class HistoryItem implements DisplayableItem {

    private static final DateFormat df = DateFormat.getDateInstance(DateFormat.LONG, LocaleUtils.getUserLocale());
    private final SearchQuery searchQuery;

    public HistoryItem(SearchQuery searchQuery) {
        this.searchQuery = searchQuery;
    }

    public String getQuery() {
        return searchQuery.query;
    }

    @Override
    public String getMainText() {
        return searchQuery.query;
    }

    private String formatDate(Date date) {
        return df.format(date);
    }

    @Override
    public String getSecondaryText() {
        return formatDate(searchQuery.timestamp);
    }

    @Override
    public String getImageURL() {
        return "";
    }

}
