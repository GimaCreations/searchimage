package com.guilmart.imagesearch.feature.search.injection;

import com.guilmart.imagesearch.core.injection.scope.PerActivity;
import com.guilmart.imagesearch.feature.search.SearchActivity;

import dagger.Component;

/**
 * Created by Ch.A.Guilmart.
 */
@PerActivity
@Component(modules = SearchModule.class)
public interface SearchComponent {
    void injectActivity(SearchActivity activity);
}
