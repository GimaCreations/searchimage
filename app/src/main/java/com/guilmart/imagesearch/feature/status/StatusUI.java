package com.guilmart.imagesearch.feature.status;

import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.guilmart.imagesearch.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Display a UI to inform the user of the data/connection status.
 * provide a "retry" button.
 * <p>
 * Created by Ch.A.Guilmart.
 */

public class StatusUI {

    @BindView(R.id.status_container)
    @Nullable
    ViewGroup container;
    @BindView(R.id.status_button)
    @Nullable
    Button button;
    @BindView(R.id.status_image)
    @Nullable
    ImageView imageView;
    @BindView(R.id.status_text)
    @Nullable
    TextView textView;
    Unbinder unbinder;

    private StatusUI() {
    }

    public StatusUI(View rootView) {
        bind(rootView);
    }

    public void bind(View rootView) {
        unbind();
        unbinder = ButterKnife.bind(this, rootView);
    }

    public void unbind() {
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    protected @StringRes
    int getText(STATUS status) {
        switch (status) {
            case CONNECTION_ERROR:
                return R.string.all_generic_error_message;
            case NO_DATA:
                return R.string.all_generic_nodata;
            default:
            case FIRST_LOADING:
                return R.string.all_generic_loading;
        }
    }

    protected @DrawableRes
    int getImage(STATUS status) {
        switch (status) {
            case CONNECTION_ERROR:
                return R.drawable.all_download_error;
            case NO_DATA:
                return R.drawable.all_cloud_no_data;
            default:
            case FIRST_LOADING:
                return R.drawable.all_loading_placeholder;
        }
    }

    protected @StringRes
    int getButtonLabel(STATUS status) {
        switch (status) {
            case CONNECTION_ERROR:
                return R.string.all_generic_retry;
            case NO_DATA:
                return R.string.all_generic_retry;
            default:
                return R.string.all_empty; // not used
        }
    }

    protected int getButtonVisibility(STATUS status) {
        switch (status) {
            case CONNECTION_ERROR:
                return View.VISIBLE;
            default:
                return View.INVISIBLE;
        }
    }

    public void display(STATUS status) {
        //
        // hide or show
        //
        boolean visible = status != STATUS.GOT_DATA;
        int visibleValue = visible ? View.VISIBLE : View.GONE;
        if (container != null) {
            container.setVisibility(visibleValue);
        }
        if (!visible) return;

        //
        // load value
        //
        if (imageView != null) {
            imageView.setImageResource(getImage(status));
        }
        if (button != null) {
            button.setVisibility(getButtonVisibility(status));
            button.setText(getButtonLabel(status));
        }
        if (textView != null) {
            textView.setText(getText(status));
        }
    }

    public void setOnClickRetry(View.OnClickListener onClickListener) {
        if (button != null) {
            button.setOnClickListener(onClickListener);
        }
    }

    public enum STATUS {
        /**
         * There is data visible on screen, no status to display (hide this element).
         */
        GOT_DATA,
        /**
         * There is no data to display because we are loading the initial data. Cannot retry
         */
        FIRST_LOADING,
        /**
         * There is no data to display because the connection failed. Can retry
         */
        CONNECTION_ERROR,
        /**
         * There is no data to display because the Repo return none. Cannot retry
         */
        NO_DATA
    }

}
