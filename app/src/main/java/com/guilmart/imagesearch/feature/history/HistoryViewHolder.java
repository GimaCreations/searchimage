package com.guilmart.imagesearch.feature.history;

import android.view.View;
import android.widget.TextView;

import com.guilmart.imagesearch.R;
import com.guilmart.imagesearch.core.base.list.DisplayableItemViewHolder;
import com.guilmart.imagesearch.core.base.view.DisplayableItem;

import butterknife.BindView;

/**
 * Created by Ch.A.Guilmart.
 */

public class HistoryViewHolder extends DisplayableItemViewHolder {

    @BindView(R.id.item_main_text)
    TextView mainText;
    @BindView(R.id.item_secondary_text)
    TextView secondaryText;

    public HistoryViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void load(DisplayableItem item) {
        mainText.setText(item.getMainText());
        secondaryText.setText(item.getSecondaryText());
    }

}
