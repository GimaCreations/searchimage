package com.guilmart.imagesearch.feature.imageloader;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.ImageView;

import com.guilmart.imagesearch.R;
import com.guilmart.imagesearch.core.utils.TextUtils;
import com.squareup.picasso.Picasso;

/**
 * Created by Ch.A.Guilmart.
 */

public class ImageLoaderProduction implements ImageLoader {
    private Context context;
    private Picasso picasso;

    public ImageLoaderProduction(Context context, Picasso picasso) {
        this.context = context;
        this.picasso = picasso;
    }

    @Override
    public void load(@NonNull ImageView iv, @NonNull String imageUrl) {
        if (TextUtils.isEmpty(imageUrl)) {
            picasso.load(R.drawable.all_loading_placeholder).into(iv);
        } else {
            picasso
                    .load(imageUrl)
                    .error(R.drawable.all_download_error)
                    //.placeholder(R.drawable.all_loading_placeholder)
                    .into(iv);
        }
    }
}