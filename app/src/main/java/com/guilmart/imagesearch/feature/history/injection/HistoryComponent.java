package com.guilmart.imagesearch.feature.history.injection;

import com.guilmart.imagesearch.core.injection.scope.PerActivity;
import com.guilmart.imagesearch.feature.history.HistoryActivity;

import dagger.Component;

/**
 * Created by Ch.A.Guilmart.
 */
@PerActivity
@Component(modules = HistoryModule.class)
public interface HistoryComponent {
    void injectActivity(HistoryActivity historyActivity);
}
