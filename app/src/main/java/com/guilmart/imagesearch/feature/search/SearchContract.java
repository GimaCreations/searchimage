package com.guilmart.imagesearch.feature.search;

import com.guilmart.imagesearch.core.base.presenter.BasePresenter;
import com.guilmart.imagesearch.core.base.view.BaseView;
import com.guilmart.imagesearch.core.base.view.DisplayableItem;
import com.guilmart.imagesearch.feature.infinitscroll.PresenterWithPager;

import java.util.List;

/**
 * Created by Ch.A.Guilmart.
 */

public interface SearchContract {
    interface VIEW extends BaseView {

        void append(DisplayableItem item);

        void setAutoComplete(List<String> list);

        void showNoMoreResult();
    }

    interface Presenter extends BasePresenter<VIEW>, PresenterWithPager {
        void navigateToDetail(DisplayableItem item);

        void navigateToHistory();

        void search(String query);

        /**
         * will trigger a call to setAutoComplete()
         */
        void requestAutoComplete();
    }
}
