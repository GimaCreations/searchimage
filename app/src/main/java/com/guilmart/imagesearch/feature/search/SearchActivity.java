package com.guilmart.imagesearch.feature.search;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;

import com.guilmart.imagesearch.BuildConfig;
import com.guilmart.imagesearch.R;
import com.guilmart.imagesearch.core.base.view.BaseMVPActivity;
import com.guilmart.imagesearch.core.base.view.DisplayableItem;
import com.guilmart.imagesearch.core.utils.AnimationsUtils;
import com.guilmart.imagesearch.core.utils.TextUtils;
import com.guilmart.imagesearch.data.socialmedia.SocialMediaRepo;
import com.guilmart.imagesearch.feature.infinitscroll.InfiniteScrollListener;
import com.guilmart.imagesearch.feature.search.injection.DaggerSearchComponent;
import com.guilmart.imagesearch.feature.search.injection.SearchComponent;
import com.guilmart.imagesearch.feature.search.injection.SearchModule;
import com.guilmart.imagesearch.feature.status.StatusUI;
import com.guilmart.imagesearch.feature.status.StatusUI.STATUS;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import timber.log.Timber;

public class SearchActivity
        extends BaseMVPActivity<SearchComponent, SearchContract.VIEW, SearchContract.Presenter>
        implements SearchContract.VIEW {

    private static final String EXTRA_QUERY = BuildConfig.APPLICATION_ID + SearchActivity.class.getSimpleName() + ".query";

    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.searchview)
    SearchView searchView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_history)
    ImageButton navigateToHistoryButton;
    @BindView(R.id.toolbar_search)
    ImageButton searchButton;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.loading)
    View loading;
    @BindView(R.id.loading_finish)
    View loading_finish;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    @Inject
    SocialMediaRepo socialMediaRepo;
    @Inject
    SearchResultAdapter searchResultAdapter;
    @Inject
    InfiniteScrollListener infiniteScrollListener;
    @BindView(android.support.v7.appcompat.R.id.search_src_text)
    SearchView.SearchAutoComplete searchAutoComplete;

    @Inject
    ArrayAdapter<String> autoCompleteAdapter;

    private StatusUI statusUI;

    public static Intent getIntent(Context fromContext, String query) {
        Intent intent = new Intent(fromContext, SearchActivity.class);
        if (!TextUtils.isEmpty(query)) {
            intent.putExtra(EXTRA_QUERY, query);
        }
        return intent;
    }

    @Override
    protected int getContentViewResId() {
        return R.layout.activity_search;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        readIntent(getIntent());

        presenter.requestAutoComplete();
    }

    protected void readIntent(@Nullable Intent intent) {
        if (intent != null && intent.getExtras() != null && intent.getExtras().containsKey(EXTRA_QUERY)) {
            String query = intent.getExtras().getString(EXTRA_QUERY, "");
            search(query);
        }
    }

    @Override
    protected void setupView() {
        //
        // Status
        //
        statusUI = new StatusUI(getRootView());
        statusUI.setOnClickRetry(view -> retry());
        statusUI.display(STATUS.NO_DATA);

        //
        // Action Bar / Toolbar / Menu
        //
        setSupportActionBar(toolbar);
        setTitle(" "); //TODO : I could use the title, but I need to hide it when the header collapse.

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                search(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        navigateToHistoryButton.setOnClickListener(view -> presenter.navigateToHistory());

        searchButton.setOnClickListener(view -> search(String.valueOf(searchView.getQuery())));

        //
        //  List
        //
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.HORIZONTAL));
        recyclerView.setAdapter(searchResultAdapter);
        recyclerView.addOnScrollListener(infiniteScrollListener);

        //
        // Auto Complete
        //
        searchView.setSuggestionsAdapter(null);
        searchView.setOnSuggestionListener(null);
        searchAutoComplete.setDropDownBackgroundResource(R.color.search_suggestion_background_text_color);
        searchAutoComplete.setOnItemClickListener((adapterView, view, itemIndex, id) -> {
            String query = (String) adapterView.getItemAtPosition(itemIndex);
            search(query);
        });
        searchAutoComplete.setAdapter(autoCompleteAdapter);

        //
        // OTHER UI ELEMENT
        //
        fab.setOnClickListener((View view) -> {
            searchView.requestFocusFromTouch();
            AnimationsUtils.tease(searchView, View.VISIBLE);
        });
    }

    private void retry() {
        if (presenter.hasNextPage()) {
            presenter.fetchNextPage();
        } else {
            if (!gotData()) {
                String query = String.valueOf(searchView.getQuery());
                if (!TextUtils.isEmpty(query)) {
                    search(query);
                }
            }
        }
    }

    private void search(String query) {
        // clear existing result
        searchResultAdapter.clear();

        // update UI with new query
        if (query != null && searchView != null && !query.equals(searchView.getQuery())) {
            searchView.setQuery(query, false);
        }
        hideKeyboard();

        // execute the query
        presenter.search(query);
    }

    @Override
    public void setAutoComplete(@NonNull List<String> list) {
        if (autoCompleteAdapter != null) {
            autoCompleteAdapter.clear();
            autoCompleteAdapter.addAll(list);
            if (searchAutoComplete != null) {
                searchAutoComplete.dismissDropDown();
            }
        }
    }

    @NonNull
    @Override
    public SearchComponent createComponent() {
        DaggerSearchComponent.Builder builder = DaggerSearchComponent.builder();
        builder.searchModule(new SearchModule(getAppComponent(), this));
        return builder.build();
    }

    @Override
    public void inject(@NonNull SearchComponent component) {
        component.injectActivity(this);
    }

    private boolean gotData() {
        return searchResultAdapter.getItemCount() > 0;
    }

    @Override
    public void showLoading() {
        loading.setVisibility(View.VISIBLE);
        STATUS status = gotData() ?
                STATUS.GOT_DATA : STATUS.FIRST_LOADING;
        statusUI.display(status);
    }

    @Override
    public void hideLoading() {
        loading.setVisibility(View.INVISIBLE);
        if (gotData()) {
            statusUI.display(STATUS.GOT_DATA);
            AnimationsUtils.tease(loading_finish, View.GONE);
        } else {
            statusUI.display(STATUS.NO_DATA);
        }
    }

    @Override
    public void showError(String message) {
        super.showError(message);
        if (!gotData()) {
            statusUI.display(STATUS.CONNECTION_ERROR);
        }
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, R.string.all_generic_error_message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public void showNoMoreResult() {
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, R.string.search_no_more_page, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public void append(DisplayableItem item) {
        Timber.d("###### APPEND 1 : %s", item.getMainText());
        searchResultAdapter.appendToList(item);
        infiniteScrollListener.onNewData();//TODO : review this use case if the user is scrolling while the data arrive
    }

}
