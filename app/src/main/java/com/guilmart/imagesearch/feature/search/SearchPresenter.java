package com.guilmart.imagesearch.feature.search;

import com.guilmart.imagesearch.core.base.presenter.BasePresenterImp;
import com.guilmart.imagesearch.core.base.view.DisplayableItem;
import com.guilmart.imagesearch.core.injection.scope.PerActivity;
import com.guilmart.imagesearch.data.history.HistoryRepo;
import com.guilmart.imagesearch.data.history.SearchQuery;
import com.guilmart.imagesearch.data.socialmedia.SocialMediaRepo;
import com.guilmart.imagesearch.feature.navigator.Navigator;
import com.guilmart.imagesearch.feature.navigator.OpenableItem;

import java.util.Date;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by Ch.A.Guilmart.
 */
@PerActivity
public class SearchPresenter
        extends BasePresenterImp<SearchContract.VIEW>
        implements SearchContract.Presenter {

    Disposable searchDisposable;
    private SocialMediaRepo socialMediaRepo;
    private HistoryRepo historyRepo;
    private Navigator navigator;

    public SearchPresenter(SocialMediaRepo socialMediaRepo, HistoryRepo historyRepo, Navigator navigator) {
        this.socialMediaRepo = socialMediaRepo;
        this.historyRepo = historyRepo;
        this.navigator = navigator;
    }

    @Override
    public void navigateToDetail(DisplayableItem item) {
        String url;
        if (item instanceof OpenableItem) {
            url = ((OpenableItem) item).getOpenUrl();
            navigator.openUrl(getContext(), url);
        }
    }

    @Override
    public void navigateToHistory() {
        navigator.history(getContext());
    }

    @Override
    public boolean hasNextPage() {
        return socialMediaRepo.hasNextPage();
    }

    @Override
    public boolean fetchNextPage() {
        if (!hasNextPage()) {
            return false;
        }
        boolean ret = socialMediaRepo.fetchNextPage();
        if (ret) {
            getView().showLoading();
        }
        return ret;
    }

    private void addToHistory(String query) {
        historyRepo.add(new SearchQuery(new Date(), query));
    }

    @Override
    public void requestAutoComplete() {
        setAutoComplete();
    }

    private void setAutoComplete() {
        getView().setAutoComplete(historyRepo.getHistoryAsString());
    }


    @Override
    public void search(String query) {

        if (searchDisposable != null && !searchDisposable.isDisposed()) {
            searchDisposable.dispose();
        }

        addToHistory(query);
        setAutoComplete();

        Observable<DisplayableItem> observable = socialMediaRepo.searchImage(query);
        if (observable == null) {
            return;
        }

        observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<DisplayableItem>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Timber.d("onSubscribe %s ", d);
                        searchDisposable = d;
                        getView().showLoading();
                    }

                    @Override
                    public void onNext(DisplayableItem displayableItems) {
                        getView().append(displayableItems);
                        getView().hideLoading(); //TODO : fix the repeated animation story
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.d("OnError %s ", e);
                        //Timber.d(e);
                        getView().hideLoading();
                        getView().showError(e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Timber.d("onComplete");
                        getView().showNoMoreResult();
                    }
                });

        Timber.d("Search %s", query);
    }
}
