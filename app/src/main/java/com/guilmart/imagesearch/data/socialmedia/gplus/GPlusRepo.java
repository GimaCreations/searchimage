package com.guilmart.imagesearch.data.socialmedia.gplus;

import com.guilmart.imagesearch.data.socialmedia.gplus.model.GPlusPost;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Ch.A.Guilmart.
 */

public interface GPlusRepo {
    Observable<List<GPlusPost>> searchPost(String query);

    /**
     * @return false if there is no nextPage or if searchTweets never succeed
     */
    boolean fetchNextPage();

    boolean hasNextPage();
}
