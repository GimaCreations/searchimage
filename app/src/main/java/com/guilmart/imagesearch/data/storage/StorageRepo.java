package com.guilmart.imagesearch.data.storage;

/**
 * Created by Ch.A.Guilmart.
 */

public interface StorageRepo {
    void put(String key, String value);

    String get(String key);

    void clear();
}
