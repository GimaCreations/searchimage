package com.guilmart.imagesearch.data.history;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

/**
 * Created by Ch.A.Guilmart.
 */

public class SearchQuery {

    @SerializedName("timestamp")
    public Date timestamp;
    @SerializedName("query")
    public String query;

    public SearchQuery(Date timestamp, String query) {
        this.timestamp = timestamp;
        this.query = query;
    }

    /**
     * are equals if they contain the an equals query
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (getClass() != o.getClass()) return false;
        // here: same class, not null:
        SearchQuery searchQuery = (SearchQuery) o;
        return Objects.equals(query, searchQuery.query);
    }

    public static class List extends ArrayList<SearchQuery> {
    }
}
