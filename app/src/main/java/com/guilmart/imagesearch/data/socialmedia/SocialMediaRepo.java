package com.guilmart.imagesearch.data.socialmedia;

import com.guilmart.imagesearch.core.base.view.DisplayableItem;

import io.reactivex.Observable;

/**
 * Return Displayable from different SocialMedia source
 * <p>
 * Created by Ch.A.Guilmart.
 */

public interface SocialMediaRepo {
    Observable<DisplayableItem> searchImage(String query);

    boolean hasNextPage();

    boolean fetchNextPage();
}
