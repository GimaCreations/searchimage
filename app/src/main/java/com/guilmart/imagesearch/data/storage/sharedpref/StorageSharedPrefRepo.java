package com.guilmart.imagesearch.data.storage.sharedpref;

import android.content.Context;
import android.content.SharedPreferences;

import com.guilmart.imagesearch.BuildConfig;
import com.guilmart.imagesearch.data.storage.StorageRepo;

/**
 * Created by Ch.A.Guilmart.
 */

public class StorageSharedPrefRepo implements StorageRepo {

    private final static String FILE_KEY = BuildConfig.APPLICATION_ID + ".StorageSharedPrefRepo.PREFERENCE_FILE_KEY";

    Context context;
    SharedPreferences sharedPreferences;

    public StorageSharedPrefRepo(Context context) {
        this.context = context;
    }

    private SharedPreferences getSharedPreferences() {
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(FILE_KEY, Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }

    @Override
    public void put(String key, String value) {
        getSharedPreferences().edit().putString(key, value).commit();
    }

    @Override
    public String get(String key) {
        return getSharedPreferences().getString(key, null);
    }

    @Override
    public void clear() {
        getSharedPreferences().edit().clear().commit();
    }
}
