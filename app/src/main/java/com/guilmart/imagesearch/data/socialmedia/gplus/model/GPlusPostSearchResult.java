package com.guilmart.imagesearch.data.socialmedia.gplus.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ch.A.Guilmart.
 */

public class GPlusPostSearchResult {

    @SerializedName("items")
    public GPlusPost[] items;
    @SerializedName("nextPageToken")
    public String nextPageToken;
    @SerializedName("selfLink")
    public String selfLink;

}
