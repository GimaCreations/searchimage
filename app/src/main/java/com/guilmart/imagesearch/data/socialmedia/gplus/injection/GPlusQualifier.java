package com.guilmart.imagesearch.data.socialmedia.gplus.injection;

import java.lang.annotation.Retention;

import javax.inject.Qualifier;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by Ch.A.Guilmart.
 */
@Qualifier
@Retention(RUNTIME)
public @interface GPlusQualifier {
}
