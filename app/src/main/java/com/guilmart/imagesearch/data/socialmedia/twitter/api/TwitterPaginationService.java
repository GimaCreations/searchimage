package com.guilmart.imagesearch.data.socialmedia.twitter.api;

import com.twitter.sdk.android.core.models.Search;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by Ch.A.Guilmart.
 */

interface TwitterPaginationService {
    // eg: "Search.searchMetadata.nextResults": "?max_id=967574182522482687&q=nasa&include_entities=1&result_type=popular",

    String SEARCH_TWEETS_BASE_URL = "/1.1/search/tweets.json";
    // note : @Path (eg: "/1.1/search/tweets.json{urlQuery}") encode the "?" as well. So wwe have to create this const.

    /**
     * @param nextResults = SEARCH_TWEETS_BASE_URL + Search.searchMetadata.nextResults
     */
    @GET
    Call<Search> tweets(@Url String nextResults);
}
