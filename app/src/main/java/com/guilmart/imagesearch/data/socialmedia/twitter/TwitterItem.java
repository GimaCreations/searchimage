package com.guilmart.imagesearch.data.socialmedia.twitter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.guilmart.imagesearch.BuildConfig;
import com.guilmart.imagesearch.core.utils.TextUtils;
import com.guilmart.imagesearch.data.socialmedia.SocialMediaItem;
import com.twitter.sdk.android.core.models.Tweet;

/**
 * Created by Ch.A.Guilmart.
 */

public class TwitterItem extends SocialMediaItem {

    private final Tweet tweet;

    public TwitterItem(@NonNull Tweet tweet) {
        this.tweet = tweet;
    }

    private static String getImageUrl(Tweet tweet) {
        String ret = "";
        try {
            ret = tweet.extendedEntities.media.get(0).mediaUrlHttps; //TODO improve this implementation
        } catch (Exception e) {
        }
        return ret;
    }

    public static boolean hasImage(Tweet tweet) {
        String imageUrl = getImageUrl(tweet);
        return !TextUtils.isEmpty(imageUrl);
    }

    @Override
    public String getMainText() {
        return tweet.text;
    }

    @Override
    public String getImageURL() {
        return getImageUrl(this.tweet);
    }

    @Nullable
    @Override
    public String getOpenUrl() {
        return String.format(BuildConfig.TWITTER_OPEN_TWEET_URL, tweet.user.screenName, tweet.idStr);
        // "https://twitter.com/statuses/" + tweet.idStr //this don't work on mobile, it redirect to mobile.twitter then return 404.
    }

}
