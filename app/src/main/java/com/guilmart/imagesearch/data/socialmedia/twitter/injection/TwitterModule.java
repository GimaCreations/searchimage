package com.guilmart.imagesearch.data.socialmedia.twitter.injection;

import android.content.Context;

import com.guilmart.imagesearch.BuildConfig;
import com.guilmart.imagesearch.core.injection.module.ContextModule;
import com.guilmart.imagesearch.core.injection.module.NetworkModule;
import com.guilmart.imagesearch.core.injection.qualifier.ApplicationContextQualifier;
import com.guilmart.imagesearch.core.injection.scope.PerApplication;
import com.guilmart.imagesearch.data.socialmedia.twitter.TwitterRepo;
import com.guilmart.imagesearch.data.socialmedia.twitter.api.TwitterApiClientCustom;
import com.guilmart.imagesearch.data.socialmedia.twitter.api.TwitterApiRepo;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

/**
 * Created by Ch.A.Guilmart.
 */
@PerApplication
@Module(includes = {NetworkModule.class, ContextModule.class})
public class TwitterModule {

    @Provides
    @PerApplication
    TwitterApiClientCustom provideTwitterApiClient(@ApplicationContextQualifier Context context, OkHttpClient okHttpClient) {

        TwitterConfig config = new TwitterConfig.Builder(context)
                //.logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig(
                        BuildConfig.TWITTER_API_KEY,
                        BuildConfig.TWITTER_API_SECRET))
                //.debug(true)
                .build();
        Twitter.initialize(config);

        TwitterApiClientCustom twitterApiClient = new TwitterApiClientCustom(okHttpClient);
        //TwitterCore.getInstance().addGuestApiClient(twitterApiClient); //it seem this is not required.

        return twitterApiClient;
    }

    @Provides
    @PerApplication
    TwitterRepo provideTwitterRepo(TwitterApiClientCustom twitterApiClient) {

        TwitterApiRepo twitterApiRepo = new TwitterApiRepo(twitterApiClient);

        return twitterApiRepo;
    }
}
