package com.guilmart.imagesearch.data.socialmedia.gplus.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ch.A.Guilmart.
 */

public class GPlusPostObject {

    @SerializedName("attachments")
    GPlusAttachment[] attachments;
}
