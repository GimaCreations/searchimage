package com.guilmart.imagesearch.data.socialmedia.twitter.api;

import com.guilmart.imagesearch.core.utils.LocaleUtils;
import com.guilmart.imagesearch.core.utils.TextUtils;
import com.guilmart.imagesearch.data.socialmedia.twitter.TwitterRepo;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Search;
import com.twitter.sdk.android.core.models.Tweet;

import java.util.List;

import io.reactivex.Emitter;
import io.reactivex.Observable;
import retrofit2.Call;
import timber.log.Timber;

/**
 * Created by Ch.A.Guilmart.
 */

public class TwitterApiRepo implements TwitterRepo {
    private static final String QUERY_PREFIX = "filter:images filter:safe ";

    TwitterApiClientCustom twitterApiClient;
    private Emitter<List<Tweet>> currentEmitter;
    private Search previousResult;

    public TwitterApiRepo(TwitterApiClientCustom twitterApiClient) {
        this.twitterApiClient = twitterApiClient;
    }

    private void setEmitter(Emitter<List<Tweet>> newEmitter) {
        if (currentEmitter != null) {
            currentEmitter.onComplete();
        }
        currentEmitter = newEmitter;
    }

    @Override
    public boolean fetchNextPage() {
        if (!hasNextPage()) return false;
        TwitterPaginationService paginationService = twitterApiClient.getPaginationService();
        String url = paginationService.SEARCH_TWEETS_BASE_URL + previousResult.searchMetadata.nextResults;
        Call<Search> tweets = paginationService.tweets(url);
        tweets.enqueue(new Callback<Search>() {
            @Override
            public void success(Result<Search> result) {
                previousResult = result.data;
                currentEmitter.onNext(result.data.tweets);
            }

            @Override
            public void failure(TwitterException exception) {
                currentEmitter.onError(exception);
            }
        });
        return true;
    }

    @Override
    public boolean hasNextPage() {
        if (currentEmitter != null && previousResult != null) {
            return !TextUtils.isEmpty(previousResult.searchMetadata.nextResults);
        }
        return false;
    }


    @Override
    public Observable<List<Tweet>> searchTweets(String query) {

        String queryImage = QUERY_PREFIX + query;
        int countPerPage = 20;
        String language = LocaleUtils.getUserLocale().getLanguage();
        Call<Search> tweets = twitterApiClient.getSearchService().tweets(queryImage, null, language, "", "", countPerPage, "", Long.MIN_VALUE, Long.MAX_VALUE, true);

        Observable<List<Tweet>> listObservable = Observable.create(
                emitter -> {
                    setEmitter(emitter);
                    tweets.enqueue(new Callback<Search>() {
                        @Override
                        public void success(Result<Search> result) {
                            previousResult = result.data;
                            emitter.onNext(result.data.tweets);
                        }

                        @Override
                        public void failure(TwitterException exception) {
                            Timber.d("failure %s", exception);
                            emitter.onError(exception);
                        }
                    });

                }

        );

        return listObservable;
    }

}
