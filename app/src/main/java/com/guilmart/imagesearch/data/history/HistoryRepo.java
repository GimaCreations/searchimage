package com.guilmart.imagesearch.data.history;

import android.support.annotation.NonNull;

import java.util.List;

/**
 * Created by Ch.A.Guilmart.
 */
public interface HistoryRepo {

    @NonNull
    List<SearchQuery> getHistory();

    void add(SearchQuery searchQuery);

    void clear();

    @NonNull
    List<String> getHistoryAsString();
}
