package com.guilmart.imagesearch.data.socialmedia.injection;

import com.guilmart.imagesearch.core.injection.scope.PerApplication;
import com.guilmart.imagesearch.data.socialmedia.SocialMediaApiRepo;
import com.guilmart.imagesearch.data.socialmedia.SocialMediaRepo;
import com.guilmart.imagesearch.data.socialmedia.gplus.GPlusRepo;
import com.guilmart.imagesearch.data.socialmedia.gplus.injection.GPlusModule;
import com.guilmart.imagesearch.data.socialmedia.twitter.TwitterRepo;
import com.guilmart.imagesearch.data.socialmedia.twitter.injection.TwitterModule;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Ch.A.Guilmart.
 */
@Module(includes = {TwitterModule.class, GPlusModule.class})
public class SocialMediaModule {

    @Provides
    @PerApplication
    SocialMediaRepo provideSocialMediaRepo(TwitterRepo twitterRepo, GPlusRepo gPlusRepo) {
        return new SocialMediaApiRepo(twitterRepo, gPlusRepo);
    }

}
