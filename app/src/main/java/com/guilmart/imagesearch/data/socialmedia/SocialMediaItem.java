package com.guilmart.imagesearch.data.socialmedia;

import com.guilmart.imagesearch.core.base.view.DisplayableItem;
import com.guilmart.imagesearch.feature.navigator.OpenableItem;

/**
 * Created by Ch.A.Guilmart.
 */

public abstract class SocialMediaItem implements DisplayableItem, OpenableItem {
    @Override
    public String getSecondaryText() {
        return "";
    }
}
