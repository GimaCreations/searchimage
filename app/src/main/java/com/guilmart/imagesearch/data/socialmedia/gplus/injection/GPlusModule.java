package com.guilmart.imagesearch.data.socialmedia.gplus.injection;

import com.guilmart.imagesearch.BuildConfig;
import com.guilmart.imagesearch.core.injection.module.ClientModule;
import com.guilmart.imagesearch.core.injection.scope.PerApplication;
import com.guilmart.imagesearch.data.socialmedia.gplus.GPlusRepo;
import com.guilmart.imagesearch.data.socialmedia.gplus.api.GPlusApiRepo;
import com.guilmart.imagesearch.data.socialmedia.gplus.api.GPlusApiService;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Ch.A.Guilmart.
 */

@Module
public class GPlusModule extends ClientModule {

    @Provides
    @PerApplication
    @GPlusQualifier
    @SuppressWarnings("SameReturnValue")
    String provideBaseUrl() {
        return BuildConfig.GPLUS_BASE_URL;
    }

    @Provides
    @PerApplication
    GPlusRepo provideGPlusRepo(GPlusApiService gPlusApiService) {
        return new GPlusApiRepo(gPlusApiService);
    }

    @Provides
    @PerApplication
    GPlusApiService provideGPlusApiService(@GPlusQualifier Retrofit retrofit) {
        return retrofit.create(GPlusApiService.class);
    }

    @Provides
    @PerApplication
    @GPlusQualifier
    Retrofit provideRetrofit(@GPlusQualifier String baseUrl, OkHttpClient client, GsonConverterFactory gsonConverterFactory, RxJava2CallAdapterFactory rxJava2CallAdapterFactory) {
        return createRetrofit(baseUrl, client, gsonConverterFactory, rxJava2CallAdapterFactory);
    }

}
