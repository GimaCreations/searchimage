package com.guilmart.imagesearch.data.socialmedia.gplus.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.guilmart.imagesearch.core.utils.TextUtils;
import com.guilmart.imagesearch.data.socialmedia.SocialMediaItem;

/**
 * Created by Ch.A.Guilmart.
 */

public class GPlusItem extends SocialMediaItem {
    private final GPlusPost post;

    public GPlusItem(GPlusPost post) {
        this.post = post;
    }

    public static boolean hasImage(GPlusPost gPlusPost) {
        if (gPlusPost != null) {
            return !TextUtils.isEmpty(gPlusPost.getAttachedImageUrl());
        }
        return false;
    }

    @Override
    public String getMainText() {
        return post.title;
    }

    @NonNull
    @Override
    public String getImageURL() {
        return post.getAttachedImageUrl();
    }

    @Nullable
    @Override
    public String getOpenUrl() {
        return post.url;
    }
}
