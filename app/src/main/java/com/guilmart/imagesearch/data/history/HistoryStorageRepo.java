package com.guilmart.imagesearch.data.history;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.guilmart.imagesearch.core.utils.TextUtils;
import com.guilmart.imagesearch.data.storage.StorageRepo;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Ch.A.Guilmart.
 */

public class HistoryStorageRepo implements HistoryRepo {
    private static final String KEY = "HistoryStorageRepo";

    StorageRepo storageRepo;

    Gson gson = new GsonBuilder()
            .create();
    SearchQuery.List searchQueries;

    public HistoryStorageRepo(StorageRepo storageRepo) {
        this.storageRepo = storageRepo;
        searchQueries = getHistory();
    }

    @NonNull
    @Override
    public SearchQuery.List getHistory() {
        String json = storageRepo.get(KEY);
        if (TextUtils.isEmpty(json)) {
            return new SearchQuery.List();
        }
        searchQueries = gson.fromJson(json, SearchQuery.List.class);
        return searchQueries;
    }

    private void addOrReplace(SearchQuery searchQuery) {
        String query = searchQuery.query;
        // searchQueries.removeIf(item -> query.equals(item.query)); //Android N and above
        ListIterator<SearchQuery> it = searchQueries.listIterator();
        for (; it.hasNext(); ) {
            if (query.equals(it.next().query)) {
                it.remove();
            }
        }
        searchQueries.add(searchQuery);
    }

    @Override
    public void add(SearchQuery searchQuery) {
        addOrReplace(searchQuery);
        String json = gson.toJson(searchQueries);
        storageRepo.put(KEY, json);
    }

    @Override
    public void clear() {
        searchQueries.clear();
        storageRepo.clear();
    }

    @NonNull
    @Override
    public List<String> getHistoryAsString() {
        SearchQuery.List qList = getHistory();
        List<String> sList = new ArrayList<>();
        for (SearchQuery query : qList) {
            sList.add(query.query);
        }
        return sList;
    }
}
