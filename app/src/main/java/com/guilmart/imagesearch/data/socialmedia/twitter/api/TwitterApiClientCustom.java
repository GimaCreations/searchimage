package com.guilmart.imagesearch.data.socialmedia.twitter.api;

import com.twitter.sdk.android.core.TwitterApiClient;

import okhttp3.OkHttpClient;

/**
 * Created by Ch.A.Guilmart.
 */

public class TwitterApiClientCustom extends TwitterApiClient {

    public TwitterApiClientCustom(OkHttpClient client) {
        super(client);
    }

    public TwitterPaginationService getPaginationService() {
        return super.getService(TwitterPaginationService.class);
    }
}
