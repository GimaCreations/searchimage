package com.guilmart.imagesearch.data.socialmedia.gplus.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ch.A.Guilmart.
 */

public class GPlusFullImage {
    @SerializedName("url")
    String url;
}
