package com.guilmart.imagesearch.data.socialmedia.gplus.api;

import com.guilmart.imagesearch.data.socialmedia.gplus.model.GPlusPostSearchResult;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Ch.A.Guilmart.
 */

public interface GPlusApiService {

    // ref: https://developers.google.com/+/web/api/rest/latest/activities/search
    // https://www.googleapis.com/plus/v1/activities
    @GET("activities")
    Observable<GPlusPostSearchResult> searchPost(@Query("key") String api_key, @Query("query") String query, @Query("language") String language, @Query("maxResults") int countPerPage, @Query("pageToken") String nextPageToken);
}
