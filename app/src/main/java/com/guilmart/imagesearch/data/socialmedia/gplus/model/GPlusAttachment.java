package com.guilmart.imagesearch.data.socialmedia.gplus.model;

import com.google.gson.annotations.SerializedName;
import com.guilmart.imagesearch.core.utils.TextUtils;

/**
 * Created by Ch.A.Guilmart.
 */

public class GPlusAttachment {
    private static final String OBJECT_TYPE_PHOTO = "photo";
    @SerializedName("objectType")
    String objectType;
    @SerializedName("fullImage")
    GPlusFullImage fullImage;

    public boolean hasImage() {
        return OBJECT_TYPE_PHOTO.equals(objectType)
                && fullImage != null
                && !TextUtils.isEmpty(fullImage.url);
    }

    public String getImageUrl() {
        if (!hasImage()) return "";
        return fullImage.url;
    }
}
