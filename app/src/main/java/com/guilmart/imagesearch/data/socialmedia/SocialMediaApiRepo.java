package com.guilmart.imagesearch.data.socialmedia;

import com.guilmart.imagesearch.core.base.view.DisplayableItem;
import com.guilmart.imagesearch.data.socialmedia.gplus.GPlusRepo;
import com.guilmart.imagesearch.data.socialmedia.gplus.model.GPlusItem;
import com.guilmart.imagesearch.data.socialmedia.gplus.model.GPlusPost;
import com.guilmart.imagesearch.data.socialmedia.twitter.TwitterItem;
import com.guilmart.imagesearch.data.socialmedia.twitter.TwitterRepo;
import com.twitter.sdk.android.core.models.Tweet;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;

/**
 * Created by Ch.A.Guilmart.
 */

public class SocialMediaApiRepo implements SocialMediaRepo {
    private final TwitterRepo twitterRepo;
    private final GPlusRepo gPlusRepo;

    ObservableTransformer<List<Tweet>, DisplayableItem> twitterMapper =
            upstream -> upstream
                    .flatMapIterable(list -> list)
                    .filter(tweet -> TwitterItem.hasImage(tweet))
                    .map(tweet -> new TwitterItem(tweet));

    ObservableTransformer<List<GPlusPost>, DisplayableItem> gPlusMapper =
            upstream -> upstream
                    .flatMapIterable(list -> list)
                    .filter(post -> GPlusItem.hasImage(post))
                    .map(post -> new GPlusItem(post));

    public SocialMediaApiRepo(TwitterRepo twitterRepo, GPlusRepo gPlusRepo) {
        this.twitterRepo = twitterRepo;
        this.gPlusRepo = gPlusRepo;
    }

    private Observable<DisplayableItem> searchImageOnTwitter(String query) {
        return twitterRepo.searchTweets(query).compose(twitterMapper);
    }

    private Observable<DisplayableItem> searchImageOnGPlus(String query) {
        return gPlusRepo.searchPost(query).compose(gPlusMapper);
    }

    private Observable<DisplayableItem> mergeAndProcess(Observable<DisplayableItem> obs1, Observable<DisplayableItem> obs2) {
        return Observable
                .mergeDelayError(obs1, obs2)
                // remove duplicated images :
                .distinct(displayableItem -> displayableItem.getImageURL());
    }

    /**
     * Return an observable of item.
     * Will terminate without error when there is no more pages to load.
     * Will terminate with error if both of the upstream failed.
     */  //TODO unit test this
    @Override
    public Observable<DisplayableItem> searchImage(String query) {
        return mergeAndProcess(searchImageOnTwitter(query), searchImageOnGPlus(query));
    }

    @Override
    public boolean hasNextPage() {
        return twitterRepo.hasNextPage() || gPlusRepo.hasNextPage();
    }

    @Override
    public boolean fetchNextPage() {
        // reminder : use "|" not "||"
        return twitterRepo.fetchNextPage() | gPlusRepo.fetchNextPage();
    }
}
