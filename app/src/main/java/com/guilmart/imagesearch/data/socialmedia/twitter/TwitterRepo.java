package com.guilmart.imagesearch.data.socialmedia.twitter;

import com.twitter.sdk.android.core.models.Tweet;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Ch.A.Guilmart.
 */

public interface TwitterRepo {
    Observable<List<Tweet>> searchTweets(String query);

    /**
     * @return false if there is no nextPage or if searchTweets never succeed
     */
    boolean fetchNextPage();

    boolean hasNextPage();
}
