package com.guilmart.imagesearch.data.socialmedia.gplus.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ch.A.Guilmart.
 */

public class GPlusPost {

    @SerializedName("title")
    String title;
    //"published":"2018-03-31T10:14:53.983Z",
    @SerializedName("published")
    String published;
    @SerializedName("id")
    String id;
    @SerializedName("url")
    String url;

    @SerializedName("object")
    GPlusPostObject object;

    String getAttachedImageUrl() {
        String ret = "";
        if (object != null && object.attachments != null) {
            GPlusAttachment[] attachments = object.attachments;
            for (GPlusAttachment attachment : attachments) {
                if (attachment != null && attachment.hasImage()) {
                    ret = attachment.getImageUrl();
                    break;
                }
            }
        }
        return ret;
    }
}
