package com.guilmart.imagesearch.data.socialmedia.gplus.api;

import android.support.annotation.Nullable;

import com.guilmart.imagesearch.BuildConfig;
import com.guilmart.imagesearch.core.utils.LocaleUtils;
import com.guilmart.imagesearch.core.utils.TextUtils;
import com.guilmart.imagesearch.data.socialmedia.gplus.GPlusRepo;
import com.guilmart.imagesearch.data.socialmedia.gplus.model.GPlusPost;
import com.guilmart.imagesearch.data.socialmedia.gplus.model.GPlusPostSearchResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.ReplaySubject;
import io.reactivex.subjects.Subject;
import timber.log.Timber;

/**
 * Created by Ch.A.Guilmart.
 */

public class GPlusApiRepo implements GPlusRepo {

    private static final String QUERY_PREFIX = "has:photo safe:on ";
    private static final String API_KEY = BuildConfig.GOOGLE_PLUS_KEY;
    private static final int MAX_PER_PAGE = 20;

    GPlusApiService gPlusApiService;
    /**
     * instance returned by getSearch()
     */
    Subject<List<GPlusPost>> subject;
    /**
     * required to fetch the next page
     */
    private GPlusPostSearchResult previousResult;
    private ObservableTransformer<GPlusPostSearchResult, List<GPlusPost>> mapper =
            upstream ->
                    upstream
                            .map(new Function<GPlusPostSearchResult, List<GPlusPost>>() {
                                @Override
                                public List<GPlusPost> apply(GPlusPostSearchResult result) throws Exception {
                                    previousResult = result;
                                    List<GPlusPost> retValue = new ArrayList<>();

                                    if (result.items != null && result.items.length > 0) {
                                        retValue = Arrays.asList(result.items);
                                    }

                                    return retValue;
                                }
                            });

    public GPlusApiRepo(GPlusApiService gPlusApiService) {
        this.gPlusApiService = gPlusApiService;
    }

    /**
     * exposed FOR TESTING purpose.
     */
    static String addPrefixToQuery(String query) {
        String queryImage = QUERY_PREFIX; // any post with image
        if (!TextUtils.isEmpty(query)) {
            queryImage = QUERY_PREFIX + query;
        }
        return queryImage;
    }

    private boolean hasNextPage(@Nullable GPlusPostSearchResult result) {
//        if (result != null) {
//            System.out.println("hasNextPage() result.nextPageToken : " + result.nextPageToken);
//        }
        return (result != null) && // got current result
                !TextUtils.isEmpty(result.nextPageToken) // result got a next page token
                // it seem that G+ return a nextPageToken
                // even if the current (and therefore the next) page is empty:
                && result.items != null     // current result are not empty
                && result.items.length > 0; // current result are not empty
    }

    private Observable<GPlusPostSearchResult> getUpstreamObservable(String query, String language, String nextPageToken) {
        return gPlusApiService.searchPost(API_KEY, query, language, MAX_PER_PAGE, nextPageToken)
                .subscribeOn(Schedulers.io());
    }

    private Observable<List<GPlusPost>> getSearch(String query) {
        String language = LocaleUtils.getUserLocale().getLanguage();
        String queryImage = addPrefixToQuery(query);
        return getUpstreamObservable(queryImage, language, "")
                .compose(mapper);
    }

    private Observable<List<GPlusPost>> getNextPage() {
        String uri = previousResult.selfLink;
        String language = TextUtils.getQueryParameterFromUri(uri, "lang");
        String q = TextUtils.getQueryParameterFromUri(uri, "query");
        String nextPageToken = previousResult.nextPageToken;

        return getUpstreamObservable(q, language, nextPageToken)
                .compose(mapper);
    }

    private Observer<List<GPlusPost>> internalObserver() {
        return new Observer<List<GPlusPost>>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(List<GPlusPost> gPlusPosts) {
                if (subject != null) {
                    subject.onNext(gPlusPosts);
                }
            }

            @Override
            public void onError(Throwable e) {
                // http error come here.
                Timber.d("internalObserver %s", e);
                //Timber.d(e);

                if (subject != null && !subject.hasComplete()) {
                    subject.onError(e);
                }
            }

            @Override
            public void onComplete() {
                Timber.d("internalObserver onComplete");
                // finish to DL one page. Was it the last one ?
                if (!hasNextPage() && subject != null && !subject.hasComplete()) {
                    subject.onComplete();
                }
            }
        };
    }

    @Override
    public Observable<List<GPlusPost>> searchPost(String query) {
        subject = ReplaySubject.create(4);
        getSearch(query).subscribe(internalObserver());
        return subject;
    }

    @Override
    public boolean fetchNextPage() {
        if (!hasNextPage()) return false;
        getNextPage().subscribe(internalObserver());
        return true;
    }

    @Override
    public boolean hasNextPage() {
        return subject != null && !subject.hasComplete()
                && hasNextPage(previousResult);
    }

}
