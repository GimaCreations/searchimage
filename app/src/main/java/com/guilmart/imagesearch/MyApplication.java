package com.guilmart.imagesearch;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import com.guilmart.imagesearch.core.injection.base.BaseAppComponent;
import com.guilmart.imagesearch.core.injection.module.ContextModule;
import com.guilmart.imagesearch.core.injection.withComponent;
import com.guilmart.imagesearch.feature.global.DaggerAppComponent;
import com.twitter.sdk.android.core.TwitterException;

import io.reactivex.exceptions.UndeliverableException;
import io.reactivex.plugins.RxJavaPlugins;
import timber.log.Timber;

import static timber.log.Timber.DebugTree;

/**
 * Created by Ch.A.Guilmart.
 */

public class MyApplication extends Application implements withComponent<BaseAppComponent> {

    private BaseAppComponent appComponent;

    public static BaseAppComponent getComponentWithContext(Context context) {
        return ((MyApplication) context.getApplicationContext()).appComponent;
    }

    private void initRx() {
        // Rx And twitter don't play nice together. Twitter throws multiple exception which end up here.
        RxJavaPlugins.setErrorHandler(e -> {
            if (e instanceof UndeliverableException) {
                e = e.getCause();
            }
            if ((e instanceof TwitterException)) {
                // the device is probably offline, and the observable already pass the first network exception.
                // we can ignore this.
                return;
            }
//            if ((e instanceof IOException)) {
//                // fine, irrelevant network problem or API that throws on cancellation
//                return;
//            }
//            if (e instanceof InterruptedException) {
//                // fine, some blocking code was interrupted by a dispose call
//                return;
//            }
            if ((e instanceof NullPointerException) || (e instanceof IllegalArgumentException)) {
                // that's likely a bug in the application
                Thread.currentThread().getUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
                return;
            }
            if (e instanceof IllegalStateException) {
                // that's a bug in RxJava or in a custom operator
                Thread.currentThread().getUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
                return;
            }
            Timber.d("ignoring Undeliverable exception");
            Timber.d(e);
        });
    }

    private void initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new DebugTree());
        }
        Timber.d("FIRST LOG of TIMBER");
    }

    private void initComponent() {
        if (appComponent == null) {
            appComponent = createComponent();
        }
        inject(appComponent);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        initTimber();
        initRx();

        initComponent();
    }

    @NonNull
    @Override
    public BaseAppComponent createComponent() {
        DaggerAppComponent.Builder builder = DaggerAppComponent.builder();
        builder.contextModule(new ContextModule(this));
        BaseAppComponent component = builder.build();
        return component;
    }

    @Override
    public void inject(@NonNull BaseAppComponent component) {
        Timber.d("appComponent class %s", component.getClass().getSimpleName());
        component.injectMyApplication(this);
    }

    @Override
    public BaseAppComponent getComponent() {
        return appComponent;
    }

    @Override
    public void setComponent(BaseAppComponent component) {
        this.appComponent = component;
    }

}
