package com.guilmart.imagesearch.core.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * easy way around Mockito limitation for static classes
 * <p>
 * Created by Ch.A.Guilmart.
 */

public class TextUtils {

    public static boolean isEmpty(String s) {
        return (s == null || "".equals(s));
    }

    @NonNull
    public static String getQueryParameterFromUri(@Nullable String uri, String name) {
        return name;//fake good enough for now
    }

}
