package com.guilmart.imagesearch.data.socialmedia.gplus.api;

import com.guilmart.imagesearch.core.utils.TestUtils;
import com.guilmart.imagesearch.data.socialmedia.gplus.GPlusRepo;
import com.guilmart.imagesearch.data.socialmedia.gplus.model.GPlusItem;
import com.guilmart.imagesearch.data.socialmedia.gplus.model.GPlusPost;

import org.junit.ClassRule;
import org.junit.Test;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * test GPlusApiRepo using a Fake (offline) service
 * <p>
 * Created by Ch.A.Guilmart.
 */
public class GPlusApiRepoTest {

    @ClassRule
    public static final TestUtils.RxImmediateSchedulerRule schedulers = new TestUtils.RxImmediateSchedulerRule();

    private static final String QUERY = "query";

    private static void p(String s) {
        System.out.println(s);
    }

    GPlusApiService getService() {
        return new GPlusApiServiceFake();
    }

    GPlusRepo getRepo() {
        return new GPlusApiRepo(getService());
    }

    @Test
    public void pagingFlowTest() {
        GPlusRepo repo = getRepo();

        //
        // initial test
        //
        assertFalse("Initial state ", repo.hasNextPage());
        assertFalse("Initial state ", repo.fetchNextPage());

        //
        // query first page
        //
        Observable<List<GPlusPost>> query = repo.searchPost(QUERY);
        TestObserver<List<GPlusPost>> tester = query.test();

        tester.assertNotTerminated();
        tester.assertNoErrors();

        assertTrue("got next page", repo.hasNextPage());

        assertEquals("only one page requested", 1, tester.values().size());
        List<GPlusPost> values = tester.values().get(0);

        assertTrue("result is not empty", values.size() >= 1);

        // GPlusApiServiceFake test (Junit self test)
        int index = 2;
        String expectedText = GPlusApiServiceFake.fakeMainText(GPlusApiRepo.addPrefixToQuery(QUERY), index);
        assertEquals("result content", expectedText, new GPlusItem(values.get(index)).getMainText());


        //
        // query next page
        //
        assertTrue("repo can fetch next page ", repo.fetchNextPage());

        tester.assertComplete();
        tester.assertNoErrors();

        assertEquals("two pages requested ", 2, tester.values().size());
        values = tester.values().get(1); // get second page

        index = 3;
        expectedText = Integer.toString(index);
        assertTrue("result content", new GPlusItem(values.get(index)).getMainText().contains(expectedText));

        // second page is last page
        tester.assertTerminated();
        tester.assertComplete();
        assertFalse("this was the last page", repo.hasNextPage());
        assertFalse("this was the last page", repo.fetchNextPage());
    }


}
