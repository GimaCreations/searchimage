package com.guilmart.imagesearch.data.socialmedia.gplus.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ch.A.Guilmart.
 */

public class GPlusPostSearchResultStub extends GPlusPostSearchResult {

    public GPlusPostSearchResultStub(List<String> labels, boolean withNextPage) {

        ArrayList<GPlusPostStub> list = new ArrayList<>();
        for (String s : labels) {
            list.add(new GPlusPostStub(s));
        }
        items = list.toArray(new GPlusPost[0]);

        if (withNextPage) {
            nextPageToken = "not empty";
        }

        selfLink = "http://www.example.com?lan=lan&query=oldQuery";
    }
}
