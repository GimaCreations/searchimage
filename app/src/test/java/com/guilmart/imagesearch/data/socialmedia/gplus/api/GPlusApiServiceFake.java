package com.guilmart.imagesearch.data.socialmedia.gplus.api;


import com.guilmart.imagesearch.core.utils.TextUtils;
import com.guilmart.imagesearch.data.socialmedia.gplus.model.GPlusPostSearchResult;
import com.guilmart.imagesearch.data.socialmedia.gplus.model.GPlusPostSearchResultStub;

import java.util.ArrayList;

import io.reactivex.Observable;

/**
 * Created by Ch.A.Guilmart.
 */

public class GPlusApiServiceFake implements GPlusApiService {

    /**
     * @param query with prefix
     */
    public static final String fakeMainText(String query, int index) {
        return query + " " + index;
    }

    private ArrayList<String> createFakeResultList(String query, int countPerPage) {
        ArrayList<String> list = new ArrayList<>();

        for (int i = 0; i < countPerPage; i++) {
            list.add(fakeMainText(query, i));
        }
        return list;
    }

    /**
     * @param query with prefix
     */
    public Observable<GPlusPostSearchResult> getFirstPage(String query, int countPerPage) {
        ArrayList<String> list = createFakeResultList(query, countPerPage);
        return Observable.just(new GPlusPostSearchResultStub(list, true));
    }

    public Observable<GPlusPostSearchResult> getLastPage(String query, int countPerPage) {
        ArrayList<String> list = createFakeResultList(query, countPerPage);
        return Observable.just(new GPlusPostSearchResultStub(list, false));
    }

    @Override
    public Observable<GPlusPostSearchResult> searchPost(String api_key, String query, String language, int countPerPage, String nextPageToken) {
        if (TextUtils.isEmpty(nextPageToken)) {
            return getFirstPage(query, countPerPage);
        } else {
            return getLastPage(query, countPerPage);
        }
    }
}
