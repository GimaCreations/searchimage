package com.guilmart.imagesearch.data.socialmedia.gplus.model;

/**
 * Created by Ch.A.Guilmart.
 */
public class GPlusPostStub extends GPlusPost {

    public GPlusPostStub(String label) {
        this.title = label;
    }

    @Override
    String getAttachedImageUrl() {
        return "http://www.example.com";
    }
}
