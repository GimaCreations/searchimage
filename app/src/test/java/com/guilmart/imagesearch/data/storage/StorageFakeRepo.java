package com.guilmart.imagesearch.data.storage;

import android.util.ArrayMap;

import java.util.Map;

/**
 * Fully Mocked, but
 * Created by Ch.A.Guilmart.
 */

public class StorageFakeRepo implements StorageRepo {
    private Map<String, String> map = new ArrayMap<>();

    @Override
    public void put(String key, String value) {
        map.put(key, value);
    }

    @Override
    public String get(String key) {
        return map.get(key);
    }

    @Override
    public void clear() {
        map.clear();
    }

}
