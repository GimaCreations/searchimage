package com.guilmart.imagesearch.data.history;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * save and return saved value, but don't remove duplicate.
 * No persistence.
 * <p>
 * Created by Ch.A.Guilmart.
 */

public class HistoryFakeRepo implements HistoryRepo {

    List<SearchQuery> list = new ArrayList<>();

    @NonNull
    @Override
    public List<SearchQuery> getHistory() {
        return list;
    }

    @Override
    public void add(SearchQuery searchQuery) {
        list.add(searchQuery);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @NonNull
    @Override
    public List<String> getHistoryAsString() {
        List<String> sList = new ArrayList<>();
        for (SearchQuery query : list) {
            sList.add(query.query);
        }
        return sList;
    }
}
