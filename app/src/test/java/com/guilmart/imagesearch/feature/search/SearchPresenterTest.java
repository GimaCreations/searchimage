package com.guilmart.imagesearch.feature.search;

import com.guilmart.imagesearch.core.base.view.DisplayableItem;
import com.guilmart.imagesearch.core.utils.TestUtils.RxImmediateSchedulerRule;
import com.guilmart.imagesearch.data.history.HistoryFakeRepo;
import com.guilmart.imagesearch.data.history.HistoryRepo;
import com.guilmart.imagesearch.data.history.SearchQuery;
import com.guilmart.imagesearch.data.socialmedia.SocialMediaRepo;
import com.guilmart.imagesearch.data.socialmedia.SocialMediaTestRepoProvider;
import com.guilmart.imagesearch.feature.navigator.Navigator;

import org.junit.ClassRule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.MockitoAnnotations;

import java.util.Date;
import java.util.List;

import static com.guilmart.imagesearch.core.utils.TestUtils.once;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNotSame;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.internal.verification.VerificationModeFactory.times;


/**
 * Created by Ch.A.Guilmart.
 */

public class SearchPresenterTest {

    @ClassRule
    public static final RxImmediateSchedulerRule schedulers = new RxImmediateSchedulerRule();
    @Captor
    ArgumentCaptor<List<String>> listStringCaptor;
    @Captor
    ArgumentCaptor<DisplayableItem> resultCaptor;

    /**
     * Test the SearchPresenter.search(query) flow (one query)
     */
    @Test
    public void searchFlowTest() {

        // init captor
        MockitoAnnotations.initMocks(this);

        //
        // create MOCK(dummy) and FAKE (working)
        //
        HistoryRepo historyRepo = spy(new HistoryFakeRepo());
        SocialMediaRepo socialMediaRepo = spy(new SocialMediaTestRepoProvider.SocialMediaFakeRepo());
        Navigator navigator = mock(Navigator.class);
        SearchContract.VIEW view = mock(SearchContract.VIEW.class);

        //
        // create OBJECT TO TEST
        //
        SearchPresenterTestable presenter = spy(new SearchPresenterTestable(socialMediaRepo, historyRepo, navigator));
        presenter.attachView(view);
        String TEST_QUERY = "test query";

        //
        // execute the TEST
        //
        presenter.search(TEST_QUERY);

        //
        // check the method called
        //

        // Presenter (Junit self test)
        // (called from the test it self. added for verifyNoMoreInteractions())
        verify(presenter, once()).attachView(any());
        verify(presenter, once()).search(eq(TEST_QUERY));
        verify(presenter, atLeastOnce()).getView(); // called internally to invoke the callback

        // added to history
        SearchQuery expected = new SearchQuery(new Date(), TEST_QUERY);
        verify(historyRepo, once()).add(eq(expected));

        // searched in Repo
        verify(socialMediaRepo, once()).searchImage(eq(TEST_QUERY));

        // return search result to the VIEW
        verify(view, once()).showLoading();
        verify(view, times(3)).hideLoading();
        verify(view, times(3)).append(resultCaptor.capture());
        verify(view, once()).showNoMoreResult();
        final List<DisplayableItem> capturedResult = resultCaptor.getAllValues();
        assertNotNull("result cannot be null", capturedResult);
        assertTrue("result should contain at least 3 items", capturedResult.size() >= 3);
        assertTrue("first result should contain the query", capturedResult.get(0).getMainText().contains(TEST_QUERY));
        assertTrue("last result should contain the query", capturedResult.get(2).getMainText().contains(TEST_QUERY));
        assertNotSame("result items should be different", capturedResult.get(0).getMainText(), capturedResult.get(1).getMainText());

        // return auto complete response to the VIEW
        verify(historyRepo, once()).getHistoryAsString(); //used internally to build the autoComplete
        verify(view, once()).setAutoComplete(listStringCaptor.capture());
        final List<String> capturedAutoComplete = listStringCaptor.getValue();
        assertNotNull("auto complete cannot be null", capturedAutoComplete);
        assertThat("should return the test query", capturedAutoComplete, hasItem(TEST_QUERY));
        assertEquals("auto complete should only have one item, because we only run one query", capturedAutoComplete.size(), 1);

        // History : was not called
        verify(historyRepo, never()).clear();
        verify(historyRepo, never()).getHistory();


        // Presenter : was not called
        verify(presenter, never()).hasNextPage();
        verify(presenter, never()).fetchNextPage();
        verify(presenter, never()).navigateToDetail(any());
        verify(presenter, never()).navigateToHistory();
        verify(presenter, never()).detachView();
        verify(presenter, never()).hasView();

        // SocialMediaRepo : was not called
        verify(socialMediaRepo, never()).hasNextPage(); // because we never called the presenter.hasNextPage()
        verify(socialMediaRepo, never()).fetchNextPage(); // because we never called the presenter.fetchNextPage()

        // Navigator : was not called
        verify(navigator, never()).history(any());
        verify(navigator, never()).openUrl(any(), any());
        verify(navigator, never()).search(any(), any());

        // VIEW : was not called
        // all the methods of the view were called once.

        verifyNoMoreInteractions(presenter);
        verifyNoMoreInteractions(historyRepo);
        verifyNoMoreInteractions(socialMediaRepo);
        verifyNoMoreInteractions(view);
        verifyNoMoreInteractions(navigator);
    }

    /**
     * Test the auto complete flow without a search
     */
    @Test
    public void autoCompleteFlowTest() {

        // init captor
        MockitoAnnotations.initMocks(this);

        //
        // create MOCK(dummy) and FAKE (working)
        //
        HistoryRepo historyRepo = spy(new HistoryFakeRepo());
        SocialMediaRepo socialMediaRepo = mock(SocialMediaRepo.class);
        Navigator navigator = mock(Navigator.class);
        SearchContract.VIEW view = mock(SearchContract.VIEW.class);

        //
        // create OBJECT TO TEST
        //
        SearchPresenterTestable presenter = spy(new SearchPresenterTestable(socialMediaRepo, historyRepo, navigator));
        presenter.attachView(view);
        String TEST_QUERY = "test query";

        //
        // execute the TEST
        //
        historyRepo.add(new SearchQuery(new Date(), TEST_QUERY));
        historyRepo.add(new SearchQuery(new Date(), TEST_QUERY+" bis"));

        presenter.requestAutoComplete();

        //
        // check the method called
        //
        verify(view).setAutoComplete(listStringCaptor.capture());
        //
        // check the returned value
        //
        assertEquals(2,listStringCaptor.getValue().size());
        assertTrue(listStringCaptor.getValue().contains(TEST_QUERY));

    }


    /**
     * required for verifyNoMoreInteractions(), because getView() is protected in the implementation
     */
    private static class SearchPresenterTestable extends SearchPresenter {
        public SearchPresenterTestable(SocialMediaRepo socialMediaRepo, HistoryRepo historyRepo, Navigator navigator) {
            super(socialMediaRepo, historyRepo, navigator);
        }

        @Override
        public SearchContract.VIEW getView() {
            return super.getView();
        }

        @Override
        public boolean hasView() {
            return super.hasView();
        }

    }
}