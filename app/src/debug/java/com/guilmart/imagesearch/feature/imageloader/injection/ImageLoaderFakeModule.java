package com.guilmart.imagesearch.feature.imageloader.injection;

import com.guilmart.imagesearch.core.injection.scope.PerApplication;
import com.guilmart.imagesearch.feature.imageloader.ImageLoader;
import com.guilmart.imagesearch.feature.imageloader.ImageLoaderFake;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Ch.A.Guilmart.
 */

@Module
public class ImageLoaderFakeModule {
    @Provides
    @PerApplication
    ImageLoader provideImageLoader() {
        return new ImageLoaderFake();
    }
}
