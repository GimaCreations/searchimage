package com.guilmart.imagesearch.feature.global;

import com.guilmart.imagesearch.core.injection.base.BaseAppComponent;
import com.guilmart.imagesearch.core.injection.module.ContextModule;
import com.guilmart.imagesearch.core.injection.module.HistoryRepoModule;
import com.guilmart.imagesearch.core.injection.scope.PerApplication;
import com.guilmart.imagesearch.data.socialmedia.injection.SocialMediaFakeModule;
import com.guilmart.imagesearch.feature.imageloader.injection.ImageLoaderFakeModule;
import com.guilmart.imagesearch.feature.navigator.injection.NavigatorModule;

import dagger.Component;

/**
 * Created by Ch.A.Guilmart.
 */
@Component(modules = {
        // Production :
        ContextModule.class,
        HistoryRepoModule.class,
        NavigatorModule.class,
        // fake : (NO dependencies on Twitter, GPlus or OkHttpClient)
        SocialMediaFakeModule.class,
        ImageLoaderFakeModule.class
})
@PerApplication
public interface AppOfflineComponent extends BaseAppComponent {
}
