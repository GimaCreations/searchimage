package com.guilmart.imagesearch.data.socialmedia.injection;

import com.guilmart.imagesearch.core.injection.scope.PerApplication;
import com.guilmart.imagesearch.data.socialmedia.SocialMediaRepo;
import com.guilmart.imagesearch.data.socialmedia.SocialMediaTestRepoProvider;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Ch.A.Guilmart.
 */

@Module
public class SocialMediaFakeModule {

    @Provides
    @PerApplication
    SocialMediaRepo provideSocialMediaRepo() {
        return new SocialMediaTestRepoProvider.SocialMediaFakeRepo();
    }

}
