package com.guilmart.imagesearch.data.socialmedia;

import android.support.annotation.NonNull;

import com.guilmart.imagesearch.core.base.view.DisplayableItem;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Ch.A.Guilmart.
 */
public class SocialMediaTestRepoProvider {

    private static String addDecorToText(String text, String diff) {
        return "bla bla " + text + " more text " + diff;
    }

    private static DisplayableItem createDummy(String query, String imageUrl, String diff) {
        DisplayableItem di = new FakeDisplayItem(addDecorToText(query, diff), imageUrl);
        return di;
    }

    private static String getUrl(String query) {
        String imageUrl = "http://www.example.com/images?q=";
        try {
            imageUrl += URLEncoder.encode(query, "UTF-8");
        } catch (UnsupportedEncodingException e) {
        }
        return imageUrl;
    }

    public static List<DisplayableItem> createDummy(String query) {
        List<DisplayableItem> list = new ArrayList<>();
        String imageUrl = getUrl(query);
        list.add(createDummy(query, imageUrl, "AA"));
        list.add(createDummy(query, imageUrl, "BB"));
        list.add(createDummy(query, imageUrl, "CC"));
        return list;
    }

    private static class FakeDisplayItem implements DisplayableItem {

        private final String query;
        private final String imageUrl;

        public FakeDisplayItem(String mainText, String imageUrl) {
            this.query = mainText;
            this.imageUrl = imageUrl;
        }

        @Override
        public String getMainText() {
            return query;
        }

        @Override
        public String getSecondaryText() {
            return "";
        }

        @NonNull
        @Override
        public String getImageURL() {
            return imageUrl;
        }
    }

    /**
     * will return item containing the query in the text and in the url query
     */
    public static class SocialMediaFakeRepo implements SocialMediaRepo {

        public static List<DisplayableItem> previousResult = new ArrayList<>();

        @Override
        public Observable<DisplayableItem> searchImage(String query) {
            previousResult = createDummy(query);
            return Observable.fromIterable(previousResult);
        }

        @Override
        public boolean hasNextPage() {
            return false;
        }

        @Override
        public boolean fetchNextPage() {
            return false;
        }

    }
}
